const apiUrl = getApp()?.globalData?.apiUrl || 'https://www.gocrazy.icu/api';
let { loadingCount } = getApp()?.globalData || 0
import polyfill from '@/utils/base64.min.js';
const { btoa } = polyfill;
function request ({
  url,
  method,
  data,
  showLoading = true,
  showErrDialog = true
}) {
  loadingCount++
  if (showLoading) {
    wx.showLoading({
      title: "加载中...",
      mask: true,
    });
  }

  return new Promise((resolve, reject) => {
    const Authorization = wx.getStorageSync('authToken') || null
    const authCode = encodeURI(`goCrazy_kidfnau_$@#_1069345${new Date().getTime()}`)
    const bae64Code = btoa(authCode)
    wx.request({
      url: apiUrl + url,
      method: method,
      data: data,
      header: {
        "auth-code": bae64Code,
        "Auth-token": Authorization,
        "Accept-Language": "zh-CN",
      },
      success: (res) => {
        if (!res.data || res.data.code === 500) {
          wx.showModal({
            title: "请求失败",
            showCancel: false,
          });
          return
        }
        if (res.data && !res.data.success) {
          if (res.data.code === "A000160") {
            // 用户未注册
            wx.showModal({
              title: res.data.message,
              showCancel: false,
              success (_res) {
                if (_res.confirm) {
                  wx.reLaunch({
                    url: "/pages/login/login",
                  })
                }
              }
            })
          } else {
            // 用户未登录或者登录过期

            if (showErrDialog && res.data.code !== "A000002") {
              wx.showModal({
                title: '提示',
                content: res.data.message,
                showCancel: false,
                complete: (res) => {
                  if (res.cancel) {
                  }
                  if (res.confirm) {

                  }
                }
              })
            }
          }
        }
        resolve(res.data);
        loadingCount--
        if (showLoading && !loadingCount) {
          wx.hideLoading();
        }
      },
      fail: (error) => {
        console.log('err', error);
        reject(error);
        if (showLoading) {
          wx.hideLoading();
        }
        wx.showModal({
          title: "请求失败",
          showCancel: false,
        });
      },
    });
  });
}
export default request;