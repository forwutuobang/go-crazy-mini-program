const trafficModeList = [
  { label: '🚶‍步行', value: 0 },
  { label: '🚲骑行', value: 1 },
  { label: '🚇公交地铁', value: 2 },
  { label: '🚗驾车', value: 3 },
  { label: '🚆火车', value: 4 },
  { label: '🚢轮船', value: 5 },
]
const NIGHTLIST = [
  { label: '不过夜', value: 0 },
  { label: '一夜', value: 1 },
  { label: '两夜', value: 2 },
  { label: '三夜', value: 3 },
  { label: '四夜', value: 4 },
  { label: '五夜', value: 5 },
  { label: '六夜', value: 6 },
  { label: '七夜', value: 7 },
  { label: '八夜', value: 8 },
  { label: '九夜', value: 9 },
  { label: '十夜', value: 10 },
  { label: '十一夜', value: 11 },
  { label: '十二夜', value: 12 },
  { label: '十三夜', value: 13 },
  { label: '十四夜', value: 14 },
  { label: '十五夜', value: 15 },
  { label: '十六夜', value: 16 },
  { label: '十七夜', value: 17 },
  { label: '十八夜', value: 18 },
  { label: '十九夜', value: 19 },
  { label: '二十夜', value: 20 },
  { label: '二十一夜', value: 21 },
  { label: '二十二夜', value: 22 },
  { label: '二十三夜', value: 23 },
  { label: '二十四夜', value: 24 },
  { label: '二十五夜', value: 25 },
  { label: '二十六夜', value: 26 },
  { label: '二十七夜', value: 27 },
  { label: '二十八夜', value: 28 },
  { label: '二十九夜', value: 29 },
  { label: '三十夜', value: 30 },
]
const DAYLIST = [
  { label: '一天', value: 1 },
  { label: '两天', value: 2 },
  { label: '三天', value: 3 },
  { label: '四天', value: 4 },
  { label: '五天', value: 5 },
  { label: '六天', value: 6 },
  { label: '七天', value: 7 },
  { label: '八天', value: 8 },
  { label: '九天', value: 9 },
  { label: '十天', value: 10 },
  { label: '十一天', value: 11 },
  { label: '十二天', value: 12 },
  { label: '十三天', value: 13 },
  { label: '十四天', value: 14 },
  { label: '十五天', value: 15 },
  { label: '十六天', value: 16 },
  { label: '十七天', value: 17 },
  { label: '十八天', value: 18 },
  { label: '十九天', value: 19 },
  { label: '二十天', value: 20 },
  { label: '二十一天', value: 21 },
  { label: '二十二天', value: 22 },
  { label: '二十三天', value: 23 },
  { label: '二十四天', value: 24 },
  { label: '二十五天', value: 25 },
  { label: '二十六天', value: 26 },
  { label: '二十七天', value: 27 },
  { label: '二十八天', value: 28 },
  { label: '二十九天', value: 29 },
  { label: '三十天', value: 30 },
]
export { trafficModeList, NIGHTLIST, DAYLIST }
