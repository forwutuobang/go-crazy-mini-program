const app = getApp()
import polyfill from '@/utils/base64.min.js';
const { btoa } = polyfill;
function uploadImage (image) {
  let imageList = []
  let uploadedImageList = []
  if (typeof image === 'string') {
    imageList.push(image)
  } else {
    imageList = image
  }
  // console.log('imageList: ', imageList);

  wx.showLoading({
    title: '图片上传中',
  })

  return new Promise((resolve, reject) => {
    // 按顺序上传
    async function uploadSequentially (index) {
      if (index < imageList.length) {
        try {
          const res = await uploadFile(imageList[index])
          uploadedImageList.push(res.data)// 将上传成功的图片 URL 存入数组
          await uploadSequentially(index + 1);// 递归调用，上传下一张图片
        }
        catch (error) {
          wx.showModal({
            title: '上传失败!',
            content: '图片上传失败，请重试',
            showCancel: false,
          })
          wx.hideLoading();
          reject(error); // 如果上传过程中出错，直接 reject
        }
      } else {
        wx.hideLoading();
        resolve(uploadedImageList);
      }
    }
    // 调用递归函数开始按顺序上传图片
    uploadSequentially(0);
  })
}

// 函数：上传单个文件并返回一个 Promise，其中 resolve 的值是上传成功后的响应对象
function uploadFile (filePath) {
  const authCode = encodeURI(`goCrazy_kidfnau_$@#_1069345${new Date().getTime()}`)
  const bae64Code = btoa(authCode)
  return new Promise((resolve, reject) => {
    wx.uploadFile({
      url: `${app.globalData.apiUrl}/travel/file/upload`,
      filePath: filePath,
      name: 'file',
      header: {
        "Auth-token": wx.getStorageSync('authToken'),
        "Accept-Language": "zh-CN",
        "auth-code": bae64Code
      },
      success: (uploadFileRes) => {
        if (uploadFileRes.statusCode === 413) {
          reject(new Error('图片上传失败，请重试'));

        }
        const res = JSON.parse(uploadFileRes?.data);
        if (!res?.success) {
          reject(new Error('图片上传失败，请重试'));
        }
        resolve(res); // 返回上传成功后的响应对象
      },
      fail: (err) => {
        reject(err); // 返回上传失败的错误对象
      }
    });
  });
}
export default uploadImage