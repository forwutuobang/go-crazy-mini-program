import request from '@/utils/request'

// 获取模板列表
export function getTemplateList (data, showLoading) {
  return request({
    url: '/travel/template/page',
    method: 'post',
    data,
    showLoading
  })
}
// 模板点赞
export function likeTravelTemplate (data) {
  return request({
    url: '/travel/template/likeTravelTemplate',
    method: 'post',
    data,
    showLoading: false,
  })
}
// 模板浏览量
export function increaseViewCount (templateId) {
  return request({
    url: `/travel/template/asyncIncreaseViewCount/${templateId}`,
    method: 'get',
    showLoading: false,
  })
}
// 新增模板
export function addTemplate (data) {
  return request({
    url: '/travel/template/save',
    method: 'post',
    data,
  })
}
// 更新编辑模版
export function editTemplate (data) {
  return request({
    url: '/travel/template/updateTemplate',
    method: 'post',
    data,
  })
}
// 复制模版
export function copyTemplate (templateId) {
  return request({
    url: `/travel/template/copy/${templateId}`,
    method: 'get',
  })
}
// 转发模版
export function shareTemplate (templateId) {
  return request({
    url: `/travel/template/forward/${templateId}`,
    method: 'get',
  })
}
// 获取模板详情
export function getTemplateDetails (data) {
  return request({
    url: `/travel/template/info`,
    method: 'get',
    data
  })
}
// 模板删除
export function templateRemove (data) {
  return request({
    url: '/travel/template/removeTemplate',
    method: 'post',
    data,
  })
}
// 获取攻略模板热搜排行榜
export function getTemplateRank (number) {
  return request({
    url: `/travel/template/hotRank/${number}`,
    method: 'get',
  })
}
// 添加内容至草稿箱
export function saveToDraft (data) {
  return request({
    url: '/travel/template/drafts/save',
    method: 'POST',
    data,
  })
}
// 查询上次草稿箱模板
export function getDraft () {
  return request({
    url: '/travel/template/drafts/get',
    method: 'get',
  })
}
// 查询上次草稿箱模板
export function deleteDraft () {
  return request({
    url: '/travel/template/drafts/remove',
    method: 'delete',
  })
}
// 模版收藏列表
export function getCollectionList (data, isShowLoading) {
  return request({
    url: '/travel/template/favorite/list',
    method: 'post',
    data,
    isShowLoading
  })
}
// 模板收藏
export function templateCollectAdd (data) {
  return request({
    url: '/travel/template/favorite/save',
    method: 'post',
    data,
  })
}

// 模板收藏删除
export function templateCollectRemove (data) {
  return request({
    url: '/travel/template/favorite/remove',
    method: 'post',
    data,
  })
}
// 模版浏览历史列表
export function getHistoryList (data, isShowLoading) {
  return request({
    url: '/travel/template/history/page',
    method: 'post',
    data,
    isShowLoading
  })
}
// 模板浏览历史删除
export function templateHistoryViewRemove (data) {
  return request({
    url: '/travel/template/history/remove',
    method: 'post',
    data,
  })
}