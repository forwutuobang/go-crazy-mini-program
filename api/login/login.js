import request from "@/utils/request";
// 登录
export function wxLogin (data) {
  return request({
    url: `/user/wxLgoin`,
    method: "post",
    data
  });
}
// 是否登录登录
export function isHadLogin (openId) {
  return request({
    url: `/user/isRegister/${openId}`,
    method: "get",
  });
}
