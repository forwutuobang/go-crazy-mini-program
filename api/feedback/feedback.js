import request from "@/utils/request";
// 提交反馈
export function submitFeedBack (data) {
  return request({
    url: '/message/feedback',
    method: "post",
    data,
  });
}