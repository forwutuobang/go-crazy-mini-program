import request from "@/utils/request";
// 获取评论列表
export function getCommentList (data) {
  return request({
    url: '/travel/template/ttComment',
    method: "get",
    data,
    showLoading: false

  });
}
// 发布评论
export function addComment (data) {
  return request({
    url: '/travel/template/ttComment/comment',
    method: "post",
    data,
  });
}
