import request from "@/utils/request";

export function getUserById (data) {
  return request({
    url: '/user/getUserById',
    method: "get",
    data
  })
}