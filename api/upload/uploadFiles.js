import request from "@/utils/request";

// 上传图片
export function uploadFile (data) {
  return request({
    url: '/travel/file/upload',
    method: "post",
    data
  })
}