import defaultImgUrl from '@/utils/defaultImg';
import { wxLogin } from '@/api/login/login';
import { getUserById } from '@/api/user/user'

import uploadImage from '@/utils/uploadImg';
import WxValidate from '@/utils/WxValidate';

Page({
  data: {
    userInfo: {
      userName: '',
      avatarUrl: defaultImgUrl,
      code: null
    },
    baseValieObj: {
      userName: {
        defaultText: '',
        errorText: '请输入用户名称',
      },
      avatarUrl: {
        defaultText: '',
        errorText: '请选取头像',
      },
    },
    errList: []
  },
  onLoad (options) {
    this.initValidate()
    wx.clearStorage()

  },
  onChooseAvatar (e) {
    console.log(e);
    if (e.detail && e.detail.avatarUrl) {
      uploadImage(e.detail.avatarUrl).then(res => {
        console.log(res);
        if (res) {
          let errList = this.data.errList
          delete errList['avatarUrl']
          this.setData({
            ['userInfo.avatarUrl']: res[0],
            errList
          })
        }
      })

    }
  },
  nameInputChange ({ detail }) {
    console.log('detail: ', detail);
    if (detail) {
      let errList = this.data.errList
      delete errList['userName']
      this.setData({
        ['userInfo.userName']: detail,
        errList
      })
    }

  },
  //验证函数
  initValidate () {
    let obj = this.data.baseValieObj
    let rules = {}
    let messages = {}
    for (const key in obj) {
      if (Object.hasOwnProperty.call(obj, key)) {
        const element = obj[key];
        rules[key] = {
          required: true
        }
        messages[key] = { required: element.errorText, defaultText: element.defaultText }
      }
    }
    console.log('rules: ', rules);
    console.log('messages: ', messages);
    this.WxValidate = new WxValidate(rules, messages)
  },
  validate (callback) {
    let errList = {}
    if (this.data.userInfo.avatarUrl === defaultImgUrl) {
      this.data.userInfo = ''
    }
    if (this.WxValidate.checkForm(this.data.userInfo)) {
      callback(true)
    } else {
      this.WxValidate.errorList.forEach(item => {
        errList[item.param] = item.msg
      })
      this.setData({
        errList
      })
    }
  },
  onLogin () {
    this.validate(valid => {
      if (valid) {
        wx.login({
          success: (res) => {
            console.log('res: ', res);
            if (res?.code) {
              this.data.userInfo.code = res.code
              wxLogin(this.data.userInfo).then(loginRes => {
                console.log(loginRes);
                if (loginRes.success) {
                  wx.setStorageSync('authToken', loginRes.data.authToken)
                  wx.setStorageSync('authTokenExpirationTime', loginRes.data.authTokenExpirationTime)
                  wx.setStorageSync('openid', loginRes.data.openid)
                  wx.setStorageSync('userId', loginRes.data.userId)
                  wx.showToast({
                    title: '登录成功',
                  })
                  getUserById({ userId: loginRes.data.userId }).then(userRes => {
                    if (userRes.success) {
                      wx.setStorageSync('userInfo', userRes.data)
                    }
                  })
                  setTimeout(() => {
                    wx.reLaunch({
                      url: '/pages/index/index',
                    })
                  }, 500);
                }
              })
            }
          },
        })
      }
    })

  }
});