Component({
  lifetimes: {
    created () { },
    ready () {
      this.initSwiperStyle()
    },
  },

  data: {
    swiperList:
      [
        [
          {
            "id": 132,
            "templateId": 60,
            "address": "武陟站",
            "nodeExpense": null,
            "longitude": "113.452057",
            "latitude": "35.088476",
            "nodeStartTime": "08:00:00",
            "nodeEndTime": "08:00:00",
            "nodeTrafficMode": null,
            "nodeResourceList": null,
            "nodeContent": "武陟站太小了，出行不方便",
            "nodeSequence": 0,
            "createTime": "2024-08-16T10:06:21.000+00:00",
            "day": 1
          }, {
            "id": 133,
            "templateId": 60,
            "address": "焦作武陟万达锦华酒店(黄河交通学院高铁站店)",
            "nodeExpense": 200.00,
            "longitude": "113.445711",
            "latitude": "35.093271",
            "nodeStartTime": "09:00:00",
            "nodeEndTime": "09:00:00",
            "nodeTrafficMode": "1",
            "nodeResourceList": null,
            "nodeContent": "200一晚，小贵，但是包早餐",
            "nodeSequence": 1,
            "createTime": "2024-08-16T10:06:21.000+00:00",
            "day": 1
          }],
        [{
          "id": 134,
          "templateId": 60,
          "address": "杨家兄弟脆皮炸鸡",
          "nodeExpense": null,
          "longitude": "113.414831",
          "latitude": "35.093419",
          "nodeStartTime": "11:00:00",
          "nodeEndTime": "11:00:00",
          "nodeTrafficMode": "1",
          "nodeResourceList": null,
          "nodeContent": "老式特色炸鸡，太好吃了",
          "nodeSequence": 2,
          "createTime": "2024-08-16T10:06:21.000+00:00",
          "day": 2
        }, {
          "id": 135,
          "templateId": 60,
          "address": "小安鱿鱼面筋",
          "nodeExpense": 50.00,
          "longitude": "113.406659",
          "latitude": "35.096876",
          "nodeStartTime": "12:00:00",
          "nodeEndTime": "15:00:00",
          "nodeTrafficMode": "1",
          "nodeResourceList": null,
          "nodeContent": "超级好吃的铁板烧",
          "nodeSequence": 3,
          "createTime": "2024-08-16T10:06:21.000+00:00",
          "day": 2
        }, {
          "id": 136,
          "templateId": 60,
          "address": "高记纯手工凉皮米皮擀面皮(兴华路二店)",
          "nodeExpense": 7.00,
          "longitude": "113.417015",
          "latitude": "35.100911",
          "nodeStartTime": "12:00:00",
          "nodeEndTime": "23:00:00",
          "nodeTrafficMode": "1",
          "nodeResourceList": null,
          "nodeContent": "武陟一大特色，太好吃了，但是卫生堪忧，苍蝇馆",
          "nodeSequence": 4,
          "createTime": "2024-08-16T10:06:21.000+00:00",
          "day": 2
        }
        ],
        [
          {
            "id": 137,
            "templateId": 60,
            "address": "武陟国贸中心",
            "nodeExpense": null,
            "longitude": "113.406598",
            "latitude": "35.105696",
            "nodeStartTime": "12:00:00",
            "nodeEndTime": "15:00:00",
            "nodeTrafficMode": "0",
            "nodeResourceList": null,
            "nodeContent": "一般般，没什么好的店，不好停车，门口特别堵",
            "nodeSequence": 5,
            "createTime": "2024-08-16T10:06:21.000+00:00",
            "day": 3
          }
        ]
      ],
    current: 0,
    title: '',
  },

  methods: {
    change (e) {
      console.log('e: ', e);
      let current = e.detail.current
      this.setData({
        current,
      })
    },
    initSwiperStyle () {
      const myAnimation = () => {
        return wx.createAnimation({
          duration: 500,
          timingFunction: 'ease',
        })
      }

      const animation1 = myAnimation()
      const animation2 = myAnimation()

      animation1.opacity(1).scale(1).step()
      animation2.opacity(0.5).scale(0.9).step()
      this.setData({
        stretchAnimation: animation1.export(),
        shrinkAnimation: animation2.export(),
      })
    },
  },
});
