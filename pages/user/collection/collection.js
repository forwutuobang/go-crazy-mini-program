import { getCollectionList, templateCollectRemove } from '@/api/template/template';
Page({
  data: {
    searchData: {
      current: 1,
      size: 10,
    }
  },
  onLoad (options) {
    this.setData({
      getCollectionList,
      templateCollectRemove
    })
  },
})