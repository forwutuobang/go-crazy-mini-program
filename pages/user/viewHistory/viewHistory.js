import { getHistoryList, templateHistoryViewRemove } from '@/api/template/template';
Page({
  data: {
    searchData: {
      current: 1,
      size: 10,
    }
  },
  onLoad (options) {
    this.setData({
      getHistoryList,
      templateHistoryViewRemove
    })
  },
})