import { templateRemove } from '@/api/template/template'

Page({
  data: {
    searchData: {
      current: 1,
      size: 10,
      cityName: "",
      userId: wx.getStorageSync('userId')
    }
  },
  onLoad (options) {
    this.setData({
      templateRemove
    })
  },
})