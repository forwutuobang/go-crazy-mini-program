import { getUserById } from '@/api/user/user'
import defaultImgUrl from '@/utils/defaultImg';

Page({

  /**
   * 页面的初始数据
   */
  data: {
    titleBarHeight: '',
    defaultAvatar: defaultImgUrl
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad (options) {
    this.setData({
      titleBarHeight: this.getSyStemTop()
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady () { },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow () {
    if (typeof this.getTabBar === 'function' && this.getTabBar()) {
      //this.getTabBar() 可以获取到当前tabbar实例
      this.getTabBar().setData({
        selected: '/pages/user/user'
      })
    }
    this.querySearch()
  },
  querySearch () {
    getUserById({
      userId: wx.getStorageSync('userId')
    }).then(res => {
      if (res.success) {
        this.setData({
          userInfo: res.data
        })
      }
    })
  },
  getSyStemTop () {
    const systemInfo = wx.getSystemInfoSync();
    const statusBarHeight = systemInfo.statusBarHeight;
    const menuButtonInfo = wx.getMenuButtonBoundingClientRect();
    const menuButtonHeight = menuButtonInfo.height + (menuButtonInfo.top - statusBarHeight) * 2;
    const titleBarHeight = statusBarHeight + menuButtonHeight;
    return titleBarHeight
  },
  userAboutTap (e) {
    console.log('e: ', e);
    let name = e.currentTarget.dataset.tag
    wx.navigateTo({
      url: `${name}/${name}`,
    })
  },
  feedbackTap () {
    wx.navigateTo({
      url: '/pages/feedback/feedback'
    })
  }
})