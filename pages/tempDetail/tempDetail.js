import {
  getTemplateDetails,
  likeTravelTemplate,
  templateCollectAdd as collectTemplateSingle,
  shareTemplate
} from '@/api/template/template'
import { NIGHTLIST, DAYLIST } from '@/utils/localData'
import { getCommentList, addComment } from '@/api/comment/comment'
const OFFSET = 0
Page({
  data: {
    templateInfo: {},
    mapVisible: false,
    isEnableScroll: true,
    posY: '',
    systemInfo: {},
    inMoveIng: false,
    SIZE: {},
    moveDisabled: true,
    commentInputValue: '',
    autoInputValue: '',
    commentList: [],
    query: {
      current: 1,
      size: 10,
      templateId: '',
      queryType: 0,
      // parentCommentId: ''
    },
    showAutoInput: false,
    isLoadingChildComment: false,
    commentChildQuantity: 0,
    userAvatarUrl: wx.getStorageSync('userInfo').avatarUrl,
  },
  onLoad (option) {
    this.handelSystemInfo()
    // const data = JSON.parse(option.data)
    this.querySearch(option)
    this.data.onLoadOption = option
  },
  // 获取模板详情 获取后再获取评论列表
  querySearch (option) {
    getTemplateDetails({ templateId: option.id, userId: wx.getStorageSync("userId") }).then((res) => {
      if (res.success) {
        // console.log('res.data: ', res.data);
        res.data.playTime && res.data.playTime.split(',').forEach((item, i) => {
          if (i === 0) {
            res.data.playTimeText = DAYLIST[item - 1].label
          } else {
            Number(item) === 0 ? '' : ',' + NIGHTLIST[item].label
          }
        })
        // console.log('res.data.playTimeText : ', res.data.playTimeText);
        this.setData({
          templateInfo: res.data,
          mapVisible: true,
        })
        // 获取评论列表
        // this.getCommentFun()
      }
    })
  },
  // 获取系统高度初始化 && move-view初始位置
  handelSystemInfo () {
    wx.getSystemInfo({
      success: (res) => {
        // console.log('res: ', res);
        console.log(' res.windowHeight: ', res.windowHeight);
        this.data.SIZE = {
          small: res.windowHeight * 0.1,
          medium: res.windowHeight * 0.5 - OFFSET,
          large: res.windowHeight * 0.9 - OFFSET,
        }
        // console.log('this.data.SIZE: ', this.data.SIZE)
        this.setData({
          systemInfo: res,
          posY: res.windowHeight * 0.8 - OFFSET,
          // posY: res.windowHeight * 0.9 - OFFSET
        })
        console.log('posY', this.data.posY);
      },
    })
  },
  //#region 模版详情可滑动区域
  // 移动开始事件
  onViewStart (e) {
    return
    // console.log('e: ', e.touches[0], e.changedTouches[0]);
    const posY = this.data.posY + OFFSET
    const startPointY = e.touches[0].clientY
    console.log('startPointY: ', startPointY)
    // 可选范围
    const limitValue = posY + 50
    if (startPointY > posY && startPointY < limitValue) {
      this.setData({
        moveDisabled: false,
      })
      this.data.startPointY = startPointY
    }
    // let moveDisabled = null
    // console.log('e.target.OFFSETTop: ', e.target.OFFSETTop);
    // if (e.target.OFFSETTop < 40) {

    //   moveDisabled = false
    //   this.data.startPointY = e.changedTouches[0].clientY
    //   console.log('start: ', this.data.startPointY);

    // } else {
    //   moveDisabled = true
    // }
    // this.setData({
    //   moveDisabled
    // })
  },
  // 滑动
  onViewChange ({ detail }) {
    this.data.posY = detail.y
    // console.log('posY: ', this.data.posY);
  },
  // 移动end事件
  onViewEnd (e) {
    if (!this.data.startPointY) return
    // console.log(e);
    let endPointY = e.changedTouches[0].clientY
    let startPointY = this.data.startPointY
    // console.log('startPointY: ', startPointY);
    // console.log('endPointY: ', endPointY);
    const systemHeight = this.data.systemInfo.windowHeight
    const SIZE = this.data.SIZE
    // console.log('posY: ', this.data.posY);
    // 如果是在最下面 点击横杠 移动至最上面
    if (
      Math.floor(this.data.posY) === Math.floor(this.data.SIZE.large) &&
      endPointY === startPointY
    ) {
      this.setData({
        posY: 0,
        isEnableScroll: false,
      })
      return
    } else if (Math.floor(this.data.posY) === 0 && endPointY === startPointY) {
      this.setData({
        posY: this.data.SIZE.large,
        isEnableScroll: true,
      })
      return
    }

    // direction =>  0:'向上滑动',1：'向下滑动'
    let direction = null
    // 开始点大于结束点 表示向上滑动 反之向下滑动
    if (startPointY > endPointY) {
      direction = 0
    } else {
      direction = 1
    }
    // 如果直接移动的点大于最小范围 设置移动区域为临界最小值
    if (this.data.posY < SIZE.small) {
      this.setData({
        posY: SIZE.small,
        isEnableScroll: false,
      })
    }
    // 如果大于最小值 小于中间值 往下滑动设置中间值 往上设置最小值
    if (this.data.posY > SIZE.small && this.data.posY < SIZE.medium) {
      this.setData({
        posY: direction ? SIZE.medium : SIZE.small,
        isEnableScroll: false,
      })
    }
    // 如果大于中间值 小于最大值 往下滑动设置最大值往上设置中间值
    if (this.data.posY > SIZE.medium && this.data.posY < SIZE.large) {
      this.setData({
        posY: direction ? SIZE.large : SIZE.medium,
        isEnableScroll: direction ? true : false,
      })
    }
    // 如果超出最大范围 设置移动区域为临界最大值
    if (this.data.posY > systemHeight || this.data.posY > SIZE.large) {
      this.setData({
        posY: SIZE.large,
        isEnableScroll: true,
      })
    }
    this.setData({
      moveDisabled: true,
    })
  },
  //#endregion


  //#region 模版点赞收藏转发操作
  // 模板点击点赞
  onTempLikesTap (e) {
    let templateInfo = this.data.templateInfo
    likeTravelTemplate({
      templateId: templateInfo.id,
      userId: templateInfo.userId,
    }).then((res) => {
      if (res.success) {
        if (templateInfo.liked) {
          templateInfo.likesCount -= 1
        } else {
          templateInfo.likesCount += 1
        }
        templateInfo.liked = !templateInfo.liked

        this.setData({
          templateInfo,
        })
      }
    })
  },
  // 模版收藏和删除
  onTempCollectionTap (e) {
    console.log('e: ', e)
    let isCollect = e.currentTarget.dataset.tag
    console.log('isCollect: ', isCollect)
    let templateInfo = this.data.templateInfo
    // let apiFunction = isCollect ? templateCollectRemove : templateCollectAdd
    collectTemplateSingle({
      templateId: templateInfo.id,
      userId: templateInfo.userId,
    }).then((res) => {
      if (res.success) {
        templateInfo.favoriteCount = templateInfo.favoriteCount || 0
        if (isCollect) {
          templateInfo.favoriteCount -= 1
        } else {
          templateInfo.favoriteCount += 1
        }
        templateInfo.favorite = !isCollect
        console.log('templateInfo: ', templateInfo)
        this.setData({
          templateInfo,
        })
      }
    })
  },

  //#endregion


  //#region 评论功能内容
  // 获取评论list
  getCommentFun () {
    getCommentList({
      ...this.data.query,
      templateId: this.data.templateInfo.id,
    }).then((res) => {
      if (res.success) {
        // console.log(res.data.records);
        if (res.data.records && res.data.records.length) {
          res.data.records.forEach((item) => {
            item.remainingReceive = item.childCount
            item.commentChildQuantity = 0
          })
          this.setData({
            commentList: res.data.records.slice().reverse(),
          })
        }
      }
    })
  },
  // 一级评论回车 ->发布一级评论
  onComInputEnter ({ detail }) {
    console.log('detail: ', detail)
    const data = {
      parentId: 0,
      templateId: this.data.templateInfo.id,
      userId: wx.getStorageSync('userId'),
      content: detail.value,
      appendFlag: 1,
    }
    if (detail.value) {
      addComment(data).then((res) => {
        if (res.success) {
          setTimeout(() => {
            wx.showToast({
              title: '发布评论成功',
              icon: 'success',
            })
          }, 200)
          this.setData({
            commentInputValue: '',
          })
          this.getCommentFun()
        }
      })
    }
  },
  // 一级评论点击，显示遮罩层，并获取评论item数据
  onCommentTap (e) {
    const commentItem = e.currentTarget.dataset.info
    console.log('commentItem: ', commentItem)

    this.setData({
      showAutoInput: true,
      commentItem,
    })
  },
  // fixed输入框 点击遮罩层部分
  handleOverlayClick () {
    this.setData({
      showAutoInput: false,
    })
  },
  // 子评论点击
  onChildrenCommentTap (e) {
    console.log(e)
    const commentItem = e.currentTarget.dataset.info
    this.setData({
      showAutoInput: true,
      commentItem,
    })
    console.log('commentItem: ', commentItem)
  },
  // 回复一级评论时的回车事件 发布二级评论
  onAutoInputEnter ({ detail }) {
    console.log('detail: ', detail)
    const isChildrencomment = this.data.commentItem.parentUserId ? true : false

    const data = {
      parentId: isChildrencomment ? this.data.commentItem.parentId : this.data.commentItem.id,
      parentUserId: this.data.commentItem.userId,
      templateId: this.data.templateInfo.id,
      userId: wx.getStorageSync('userId'),
      content: detail.value,
      appendFlag: 1,
    }
    if (detail.value) {
      addComment(data).then((res) => {
        if (res.success) {
          setTimeout(() => {
            wx.showToast({
              title: '发布评论成功',
              icon: 'success',
            })
          }, 100)
          this.setData({
            autoInputValue: '',
            showAutoInput: false,
          })
          if (isChildrencomment) {
            const currentComment = this.data.commentList.find(
              (item) => item.id === this.data.commentItem.parentId
            )

            currentComment.queryFrequency = Math.floor(
              currentComment.children.length / 10
            )
            // 搁置 有问题 currentComment.queryFrequency = Math.floor(currentComment.children.length / 10)  --------------------------------------------------------------------------------
            console.log(
              'Math.floor(currentComment.children.length / 10): ',
              Math.floor(currentComment.children.length / 10)
            )
            const currentIndex = this.data.commentList.findIndex(
              (item) => item.id === this.data.commentItem.parentId
            )
            // currentComment.children.push()
            this.handleGetChildComment({
              target: {
                dataset: {
                  info: currentComment,
                  index: currentIndex,
                },
              },
            })
          } else {
            this.getCommentFun()
          }
        }
      })
    }
  },
  handleGetChildComment (e) {
    const commentInfo = e.target.dataset.info
    console.log('commentInfo: ', commentInfo)
    const commentIndex = e.target.dataset.index
    let query = {
      current: commentInfo.queryFrequency + 1 || 1,
      size: 10,
      templateId: this.data.templateInfo.id,
      queryType: 1,
      parentCommentId: commentInfo.id,
    }
    // 按钮loading状态
    this.setData({
      [`commentList[${commentIndex}].isLoadingChildComment`]: true,
    })
    // 获取评论的子评论
    getCommentList(query).then((res) => {
      if (res.success) {
        console.log(res)
        if (res.data.records && res.data.records.length) {
          // 设置查询次数 每次加一
          commentInfo.queryFrequency = commentInfo.queryFrequency ? commentInfo.queryFrequency + 1 : (commentInfo.queryFrequency = 1)
          if (commentInfo.queryFrequency !== 1) {
            res.data.records.forEach((item) => {
              commentInfo.children.push(item)
            })
          } else {
            commentInfo.children = res.data.records
          }

          commentInfo.remainingReceive -= res.data.records.length
          const animation = wx.createAnimation({
            duration: 200,
            timingFunction: 'ease-in-out',
          })
          // commentInfo.commentChildQuantity = commentInfo.commentChildQuantity || 0
          commentInfo.commentChildQuantity += res.data.records.length
          animation.height(commentInfo.commentChildQuantity * 60).top(0).step({ duration: 300 }).height('auto').opacity(1).step()
          this.setData({
            [`commentList[${commentIndex}]`]: commentInfo,
            isLoadingChildComment: false,
          })
          setTimeout(() => {
            this.setData({
              [`commentList[${commentIndex}].comAnimation`]: animation.export(),
            })
          })
        }
      }
    })
  },
  //#endregion

  // 模版转发
  onShareAppMessage () {
    shareTemplate(this.data.templateInfo.id).then(res => {
      this.onLoad(this.data.onLoadOption)
    })
    return {
      title: this.data.templateInfo.templateName,
    }
  },
  // 跳转原作者模版
  toOriginTemp () {
    wx.navigateTo({
      url: `/pages/tempDetail/tempDetail?id=${this.data.templateInfo.originalTemplateId}`,
    })
  }
})
