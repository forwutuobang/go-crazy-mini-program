Page({
  data: {
    searchData: {
      current: 1,
      size: 10,
      cityName: "",
    },
    checkName: 'new'
  },
  onLoad () {
    // wx.navigateTo({
    //   url: '/pages/createTemp/createTemp',
    // })
    // wx.switchTab({
    //   url: '/pages/user/user',
    // })
    // this.getTabBar().setData({
    //   selected: '/pages/index/index'
    // })
    // this.connect(); // 调用连接函数
  },

  connect () {
    // const requestTask = wx.request({
    //   url: getApp().globalData.apiUrl + '/message/sse/subscribe/7', // 需要请求的接口地址
    //   enableChunked: true, // enableChunked必须为truex
    //   header: {
    //     "Auth-token": wx.getStorageSync('authToken') || null,
    //     "Accept-Language": "zh-CN",
    //     'content-type': 'text/event-stream'
    //   },
    //   method: "GET",
    //   timeout: 999999999,
    //   success (res) {
    //     console.log('请求success', res)
    //   },
    //   fail: function (error) {
    //     // 请求失败的操作
    //     console.error(error);
    //   },
    // })
    // // 监听服务端返回的数据
    // requestTask.onChunkReceived(res => {
    //   console.log('onChunkReceived返回的数据', res);
    //   arrayBufferToString(res.data)
    // })
    // function arrayBufferToString (arr) {
    //   const uint8Array = new Uint8Array(arr); // 示例的 Uint8Array 数据
    //   const decoder = new TextDecoder('utf-8'); // 指定编码方式，这里假设是 UTF-8
    //   const decodedString = decoder.decode(uint8Array);
    //   return decodedString;
    // }

    // var socketTask = null;
    // socketTask = wx.connectSocket({
    //   url: getApp().globalData.apiUrl + '/message/sse/subscribe/7', // 替换成你的服务器端点
    //   header: {
    //     "Auth-token": wx.getStorageSync('authToken') || null,
    //     "Accept-Language": "zh-CN",
    //     'content-type': 'application/json'
    //   },
    //   success: function () {
    //     console.log('WebSocket连接成功');
    //   },
    //   fail: err => {
    //     console.log(err);
    //   }
    // });
    // console.log('socketTask: ', socketTask);
    // socketTask.onOpen(function (res) {
    //   console.log('WebSocket连接已打开！');
    // });

    // socketTask.onMessage(function (res) {
    //   console.log('收到服务器内容：' + res.data);
    //   // 在这里处理服务器发送的数据
    // });

    // socketTask.onError(function (res) {
    //   console.log('WebSocket连接打开失败，请检查！');
    // });

    // socketTask.onClose(function (res) {
    //   console.log('WebSocket连接已关闭！');
    // });
  },

  onSearchTap () {
    wx.navigateTo({
      url: '/pages/searchCity/searchCity',
    })
  },
  onTabTap (e) {
    console.log('e.currentTarget: ', e.currentTarget);
    const name = e.currentTarget.dataset.name
    let query = {
      current: 1,
      size: 10,
      sortColumn: 'pageView'
    }
    if (name === "new") {
      delete query['sortColumn']
    }
    this.selectComponent("#home-page").querySearch(query, () => {
      this.setData({
        checkName: name,
        searchData: query
      })
    }, true, true)

  }
})
