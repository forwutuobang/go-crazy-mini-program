import { trafficModeList, NIGHTLIST, DAYLIST } from '@/utils/localData'
import allCitys from '@/utils/allCity'
import {
  addTemplate,
  saveToDraft,
  getDraft,
  deleteDraft,
  getTemplateDetails,
  editTemplate,
  copyTemplate
} from '@/api/template/template'
import WxValidate from '@/utils/WxValidate'
const NIGHTLASTLENGTH = 3
Page({
  data: {
    tempData: {}, // 模板全部数据
    citys: [], // 城市picker列表
    cityVisible: false, // 城市picker是否显示
    selectedCityName: '', //选择的模板城市名称
    tempStepList: [],
    nodePopupVisible: false, // 节点popup是否显示
    currentStepsDate: {}, // 当前节点项全部数据
    isStartTime: '', // 当前日期时间状态是否开始时间
    minuteVisible: false, // 节点开始结束picker是否显示
    defaultMinutes: '08：00', // 点击日期的时间数据,
    popupPropsObj: {
      zIndex: 12100,
      overlayProps: {
        zIndex: 11999,
      },
    },
    showTrafficMode: false,
    limitStart: '',
    limitEnd: '',
    trafficPickerList: trafficModeList,
    baseValieObj: {
      templateName: {
        defaultText: '',
        errorText: '输入模板标题',
      },
      cityName: {
        defaultText: '请选择城市',
        errorText: '请选择城市',
      },
      // playTime: {
      //   defaultText: '总游玩时间',
      //   errorText: '请输入游玩时间',
      // },
      mainImageUrl: {
        text: '请上传封面图片',
        errorText: '请上传封面图片',
      },
    },
    validatePopupObj: {
      nodeTrafficMode: {
        defaultText: '',
        errorText: '请输入交通方式',
      },
      address: {
        defaultText: '',
        errorText: '请输入游玩地址',
      },
      // nodeStartTime: {
      //   defaultText: '',
      //   errorText: '请选择开始时间',
      // },
      // nodeEndTime: {
      //   defaultText: '',
      //   errorText: '请选择结束时间',
      // },

      //   nodeResourceList: {
      //     defaultText: '',
      //     errorText: '请上传地址图集',
      //   },
    },
    errList: [],
    dayList: DAYLIST,
    nightList: [{ label: '一夜', value: 1 }],
    playTimePopupVisible: false,
    cityInitKeys: {
      label: 'value',
      value: 'value',
    },
    trafficInitKeys: {
      label: 'label',
      value: 'value',
    },
    createTempTitle: '',
    isFormEdit: false,
    isReadOnly: false,
    showStepSwiper: false,
    titleText: '新增景点模版',
    submitText: '发布'
  },

  onLoad (context) {
    console.log('context: ', context);

    if (context.templateId) {
      getTemplateDetails({ templateId: context.templateId, userId: wx.getStorageSync("userId") }).then(res => {
        if (res.success) {
          this.initHasData(res.data)
          this.setData({
            showStepSwiper: true
          })
        }
      })
      this.setData({
        isFormEdit: true,
        titleText: '编辑景点模版',
        submitText: '更新模版'
      })
    } else {
      this.handleGetDraft()
      this.setData({
        showStepSwiper: true
      })
    }
    if (context.isReadOnly) {
      this.setData({
        isReadOnly: context.isReadOnly,
        titleText: '查看景点模版'
      })
    }
    if (context.isCopy) {
      this.setData({
        // isReadOnly: context.isReadOnly,
        titleText: '复制景点模版',
        submitText: '复制并发布',
        isCopy: context.isCopy
      })
    }
    this.initValidate(true)
    this.initCityList()
  },

  // #region 草稿箱方法
  // 获取草稿箱内容
  handleGetDraft () {
    getDraft().then((res) => {
      if (res.success) {
        // 有草稿箱内容
        if (res.data) {
          // 设置景点stepList
          this.initHasData(res.data)
        }
      }
    })
  },
  initHasData (data) {
    data.nodeDTOList && data.nodeDTOList.length
      ? (this.data.tempStepList = data.nodeDTOList)
      : (this.data.tempStepList = [[]])
    this.setData({
      tempData: { ...data, mainImageUrl: data.mainImageUrl ? [data.mainImageUrl] : null, },
      tempStepList: this.data.tempStepList,
      selectedCityName: data.cityName || '', // 设置选择城市列表显示text
      // playTimeText: data.playTime, // 设置游玩时间列表显示text
      ['tempData.cityName']: data.cityName,
      // ['tempData.playTime']: OriginPlayTime,
    })
  },
  // 点击左上角返回方法 未编辑内容是否保存草稿箱
  handleBack () {
    console.log(this.getPostData())
    if (!this.data.isFormEdit) {
      if (this.getPostData().templateName) {
        wx.showModal({
          content: '是否保留此次编辑',
          cancelText: '不保留',
          confirmText: '帮我保留',
          complete: (res) => {
            if (res.cancel) {
              deleteDraft().then((res) => {
                if (res.success) {
                  wx.navigateBack()
                }
              })
            }
            if (res.confirm) {
              saveToDraft(this.getPostData()).then((res) => {
                if (res.success) {
                  wx.showToast({
                    title: '保存成功',
                  })
                  wx.navigateBack()
                }
              })
            }
          },
        })
      } else {
        wx.navigateBack()
      }
    } else {
      wx.navigateBack()

    }

  },
  // #endregion

  // #region 模板标题方法
  // 模板标题输入change
  onTitleInputChange ({ detail }) {
    let errList = { ...this.data.errList }
    //如果输入内容 删除errList中对应的key
    // 没有输入内容 errList中添加对应的key
    detail.value
      ? delete errList['templateName']
      : (errList.templateName = true)
    this.setData({
      ['tempData.templateName']: detail.value,
      errList,
    })
  },
  //#endregion

  //#region 选择城市方法
  // cell点击选择城市
  onCityPicker () {
    if (this.data.isReadOnly) return

    this.setData({ cityVisible: true })
  },
  // 初始化城市
  initCityList () {
    let citys = []

    citys = allCitys.map((item) => {
      // 一级数据（北京市级别）
      let topLevel = {
        code: item.code,
        value: item.value,
      }
      // 一级 children 数据（各区级别）
      // const children = item.children.map(child => ({
      //   code: child.code,
      //   value: child.value,
      // }));
      item.children.forEach((child) => {
        if (child.value !== item.value) {
          topLevel.children = topLevel.children || []
          topLevel.children.push({
            code: child.code,
            value: child.value,
          })
        }
      })
      // 合并一级数据和一级 children 数据到一个对象中
      return { ...topLevel }
    })
    this.setData({
      citys,
    })
  },
  // 城市选择完成
  onChange ({ detail }) {
    // let selectedCityName = detail.selectedOptions.map((item) => item.value).join('/')
    let selectedCityName = detail.value
    let errList = { ...this.data.errList }
    delete errList['cityName']
    this.setData({
      selectedCityName: selectedCityName,
      ['tempData.cityName']: selectedCityName, // 设置picker再次打开默认位置是之前选中的  value是['北京市']
      errList,
    })
  },
  //#endregion

  //#region 游玩时间方法
  // 游玩时间change
  // cell选择游玩时间
  onPlayTimePicker () {
    // 如果有night数,设置nightList为nigh数量
    let nightList = []
    let spliceCount = 0
    let nightIndex = 0

    if (!this.data.tempData.playTime) {
      spliceCount = NIGHTLASTLENGTH
      nightList = NIGHTLIST.slice(0, spliceCount)
    } else {
      nightIndex = this.data.tempData.playTime?.[1] || NIGHTLASTLENGTH
      nightList = NIGHTLIST.slice(0, nightIndex + 1)
    }

    this.setData({
      playTimePopupVisible: true,
      nightList,
    })
  },
  // 游玩时间选择完成
  onPlayTimeInputChange ({ detail }) {
    let errList = { ...this.data.errList }
    // 如果输入内容 删除errList中对应的key
    detail.value ? delete errList['playTime'] : (errList.playTime = true)
    this.setData({
      ['tempData.playTime']: detail.value,
      errList,
    })
  },
  //#endregion

  // 花费输入change
  onPriceInput ({ detail }) {
    const isNumber = /^\d+(\.\d+)?$/.test(detail.value)
    let errList = { ...this.data.errList }
    if (!isNumber) {
      errList.nodeExpense = {}
      errList.nodeExpense.errText = '请输入正确的数字'
    } else {
      delete errList['nodeExpense']
    }
    this.setData({
      ['currentStepsDate.nodeExpense']: detail.value,
      errList,
    })
  },

  // 创建新节点
  createNewNode ({ detail }) {
    // if (!this.data.tempData.cityName) {
    //   let errList = { ...this.data.errList }
    //   errList.cityName = true
    //   this.setData({
    //     errList
    //   })
    //   getApp().globalData.Message.info({
    //     context: this,
    //     offset: [90, 90],
    //     duration: 3000,
    //     theme: 'info',
    //     // single: false, // 打开注释体验多个消息叠加效果
    //     content: `请先选择模板城市`,
    //   })
    //   return
    // }
    // 不是第一个节点
    let createTempTitle = ''
    createTempTitle = `创建景点 (第 ${detail.dayNumber + 1} 天,第 ${detail.dayPlaceNumber + 1} 个景点)`
    this.setData({
      createTempTitle,
    })
    this.data.dayIndex = detail.dayNumber
    this.data.dayPlaceIndex = detail.dayPlaceNumber
    if (detail.dayPlaceNumber > 0) {
      this.setData({
        showTrafficMode: true,
        currentStepsDate: {},
      })
    } else {
      // 第一个节点
      this.setData({
        currentStepsDate: {},
        showTrafficMode: false,
      })
    }
    this.setNodePopupVisible(true)
  },

  // 点击step步骤项
  onStepsTap ({ detail }) {
    let currentStepsDate = detail.data
    this.setData({
      createTempTitle: this.data.isReadOnly ? '查看景点' : '编辑景点',
      currentStepsDate,
      dayPlaceIndex: detail.dayPlaceNumber,
      // 如果大于0 表示是第二天往后 显示交通方式
      showTrafficMode: detail.dayPlaceNumber > 0 ? true : false,
    })
    this.setNodePopupVisible(true)
  },
  // deleteStepItem (e) {
  //   const index = e.currentTarget.dataset.index
  //   const item = e.currentTarget.dataset.item
  //   wx.showModal({
  //     title: `确定删除景点`,
  //     content: item.address,
  //     complete: (res) => {
  //       if (res.confirm) {
  //         this.data.tempStepList.splice(index, 1)
  //         this.setData({
  //           tempStepList: this.data.tempStepList,
  //         })
  //       }
  //     },
  //   })
  // },
  // 文件上传组件 选择图片上传后trigger方法
  onStepImgUploaded ({ detail }) {
    let errList = { ...this.data.errList }
    let nodeResourceList = this.data.currentStepsDate.nodeResourceList || []

    delete errList['nodeResourceList']
    // 添加每个图片链接
    detail.forEach((item) => {
      nodeResourceList.push(item)
    })
    // currentStepsDate.nodeResourceList设置后会添加图片到图片组件里 // 不稳妥
    this.setData({
      ['currentStepsDate.nodeResourceList']: nodeResourceList,
      errList,
    })
  },
  // 文件上传组件 选择图片上传后trigger方法
  onMainImgUploaded ({ detail }) {
    let errList = { ...this.data.errList }
    delete errList['mainImageUrl']
    this.setData({
      ['tempData.mainImageUrl']: detail,
      errList,
    })
  },

  /*                                                          popup方法                                                                      */

  setNodePopupVisible (value = false) {
    if (typeof value !== 'boolean') {
      value = false
    }
    this.setData({
      nodePopupVisible: value,
    })
  },
  showTrafficPicker () {
    if (this.data.isReadOnly) return

    this.setData({
      trafficVisible: true,
    })
  },
  onTrafficConfirm ({ detail }) {
    let errList = { ...this.data.errList }
    detail ? delete errList['nodeTrafficMode'] : (errList.nodeTrafficMode = true)
    this.setData({
      ['currentStepsDate.nodeTrafficMode']: detail.value[0],
      errList,
    })
  },

  onPickerChange ({ detail }) {
    this.setData({
      playTimeText: detail.label,
      ['tempData.playTime']: detail.value,
    })
  },
  onColumnChange ({ detail }) {
    if (detail.column === 0 && detail.index !== 0) {
      let nightList = NIGHTLIST.slice(0, detail.index + NIGHTLASTLENGTH)
      this.setData({
        nightList,
      })
    } else if (detail.index === 0 && detail.value[0] === 1) {
      let nightList = NIGHTLIST.slice(0, NIGHTLASTLENGTH)
      this.setData({
        nightList,
      })
    }
  },
  onNodeHidden () {
    this.setData({
      errList: {},
    })
    // this.setNodePopupVisible(false)
    // 如果有任意一项输入 并且不是只读 就提示 否则不提示直接关闭
    if (Object.keys(this.data.currentStepsDate).length > 0 && !this.data.isReadOnly) {
      wx.showModal({
        title: '关闭窗口',
        content: '放弃编辑节点？',
        complete: (res) => {
          if (res.cancel) {
          }
          if (res.confirm) {
            this.setNodePopupVisible(false)
          }
        },
      })
    } else {
      this.setNodePopupVisible(false)
    }
  },

  nodeContentInputChange ({ detail }) {
    let errList = { ...this.data.errList }
    detail ? delete errList['nodeContent'] : (errList.nodeContent = true)
    this.setData({
      ['currentStepsDate.nodeContent']: detail,
      errList,
    })
  },

  // nealyby组件点击地址trigger方法
  addressInfo ({ detail }) {
    let errList = { ...this.data.errList }
    let currentStepsDate = Object.assign(this.data.currentStepsDate, {
      ...detail,
    })
    detail ? delete errList['address'] : (errList.address = true)
    this.setData({
      currentStepsDate,
      errList,
    })
  },

  // 显示日期选择
  // defaultMinutes每次显示根据当前选择是开始还是结束字段显示值
  // limitStart & limitEnd 设置选择最大值最小值，根据已经选择的开始日期和结束日期判断

  getMinutesHandle (minute, number = 2) {
    let today = new Date()
    // 设置小时，分钟、秒和毫秒
    today.setHours(Number(minute.trim().slice(0, 2)), 0, 0, 0);

    // 加上两小时
    today.setHours(today.getHours() + number);
    return today.toTimeString().slice(0, 5)
  },
  showMinutePicker (e) {
    if (this.data.isReadOnly) return
    const isStartTime = e.target.dataset.type ?? e.currentTarget.dataset.type

    let nodeStartTime = this.data.currentStepsDate.nodeStartTime
    let nodeEndTime = this.data.currentStepsDate.nodeEndTime

    if (!isStartTime && !nodeStartTime) {
      getApp().globalData.Message.warning({
        context: this,
        offset: [90, 90],
        duration: 3000,
        theme: 'warning',
        // single: false, // 打开注释体验多个消息叠加效果
        content: '请先选择开始时间!',
      })
      return
    }
    let defaultMinutes = ''
    // defaultMinutes:''
    console.log('nodeStartTime: ', nodeStartTime);


    let defaultStartTime = '8:00'
    let defaultEndTime = '12:00'

    let limitStart = ''
    let limitEnd = ''

    let dayList = this.selectComponent('#k-swiper').getSingleDayData()
    console.log('dayList: ', dayList);

    console.log('this.data.dayPlaceIndex : ', this.data.dayPlaceIndex);

    let lastDay = dayList[this.data.dayPlaceIndex - 1]
    let nextDay = dayList[this.data.dayPlaceIndex + 1]

    console.log('lastDay: ', lastDay);
    // 也可能新增后又点击了刚新增的数据 lastDay会是undefind
    if (lastDay) {
      defaultStartTime = lastDay.nodeEndTime
      limitStart = '2000-01-01 ' + lastDay.nodeEndTime
      console.log('nextDay: ', nextDay);
      // 也可能新增后又点击了刚新增的数据 lastDay会是undefind
    }
    if (nextDay) {
      defaultEndTime = nextDay.nodeEndTime

      limitEnd = '2000-01-01 ' + nextDay.nodeEndTime
    }
    // else if (dayList.length >= 3) {
    //   let nextDay = dayList[this.data.dayPlaceIndex + 1]
    //   console.log('nextDay: ', nextDay);
    //   // 也可能新增后又点击了刚新增的数据 lastDay会是undefind
    //   if (nextDay) {
    //     defaultEndTime = nextDay.nodeEndTime

    //     limitEnd = '2000-01-01 ' + nextDay.nodeEndTime
    //   }
    // }

    // 有type是开始时间 否则结束时间
    if (isStartTime) {
      defaultMinutes = nodeStartTime ?? defaultStartTime// 设置选择日期时默认时间
      this.setData({
        limitEnd: nodeEndTime
          ? '2000-01-01 ' + nodeEndTime
          : '',
        limitStart: limitStart,
      })
    } else {
      if (nodeStartTime) {
        defaultEndTime = this.getMinutesHandle(nodeStartTime)
      }
      defaultMinutes = nodeEndTime ?? defaultEndTime
      this.setData({
        limitStart: nodeStartTime
          ? '2000-01-01 ' + nodeStartTime
          : '',
        limitEnd: limitEnd,
      })
    }
    this.setData({
      minuteVisible: true,
      isStartTime,
      defaultMinutes,
    })
  },

  // 选择日期确认
  onMinuteConfirm ({ detail }) {
    let errList = { ...this.data.errList }
    const isStartTime = this.data.isStartTime
    delete errList[isStartTime ? 'nodeStartTime' : 'nodeEndTime']
    this.setData({
      [`currentStepsDate.${isStartTime ? 'nodeStartTime' : 'nodeEndTime'}`]:
        detail.value,
      errList,
    })
    console.log('errList: ', errList)
  },
  appointmentChange (e) {
    this.setData({
      ['currentStepsDate.appointment']: e.detail.value,
    });
  },
  //#region 单个节点保存
  onNodeSubmit () {
    console.log('this.data.currentStepsDate: ', this.data.currentStepsDate)
    this.initValidate(false)
    this.validata(false, (valid) => {
      if (valid) {
        // 设置swiper数据
        // 获取当前所有金额
        const currentDay = this.selectComponent('#k-swiper').getSingleDayData()
        console.log('currentDay: ', currentDay);
        // 地址重复
        let duplicateAddress = currentDay.find(item => item.address === this.data.currentStepsDate.address)
        if (duplicateAddress) {
          getApp().globalData.Message.info({
            context: this,
            offset: [90, 90],
            duration: 3000,
            theme: 'warning',
            // single: false, // 打开注释体验多个消息叠加效果
            content: `游玩地址不能重复`,
          })
          return
        }
        // 设置swiper当前数据
        this.selectComponent('#k-swiper').setSwiperListItem(this.data.currentStepsDate)
        // 获取当前总花费
        const allExpense = this.selectComponent('#k-swiper').calculateDailyExpenses()
        this.setData({
          ['tempData.expense']: allExpense,
          showTrafficMode: false,
        })
        this.setNodePopupVisible(false)
      }
    })
  },
  //#endregion
  //#region 验证函数
  // 初始化验证函数
  initValidate (isBase) {
    let obj = isBase ? this.data.baseValieObj : this.data.validatePopupObj
    let rules = {}
    let messages = {}
    for (const key in obj) {
      if (Object.hasOwnProperty.call(obj, key)) {
        const element = obj[key]
        rules[key] = {
          required: true,
        }
        messages[key] = {
          required: element.errorText,
          defaultText: element.defaultText,
        }
      }
    }
    // 如果是第一个step 去掉对交通方式的校验
    if (!isBase) {
      // 第一个节点
      if (!this.data.showTrafficMode) {
        for (const key in rules) {
          key === 'nodeTrafficMode' ? (rules[key].required = false) : ''
        }
      }
    }
    // const rules = {
    //   templateName: {
    //     required: true,
    //   },
    //   cityName: {
    //     required: true,
    //   },
    //   playTime: {
    //     required: true,
    //   },
    //   mainImageUrl: {
    //     required: true,
    //   },
    // }
    // const messages = {
    //   templateName: {
    //     required: '请输入模板标题',
    //     defaultText: '一个引人入胜的标题',
    //   },
    //   cityName: {
    //     required: "请选择城市",
    //     defaultText: '',
    //   },
    //   playTime: {
    //     required: "请输入游玩时间",
    //     defaultText: '总游玩时间，在此不需要输入，会根据各景点花费累计',
    //   },
    //   mainImageUrl: {
    //     required: "请上传封面图片",
    //     defaultText: ''
    //   },
    // }

    this.WxValidate = new WxValidate(rules, messages)
    // 自定义验证规则
    // this.WxValidate.addMethod('IDcardImages', (value, param) => {
    //   return this.WxValidate.optional(value) || value.length == 2
    // }, '请上传身份证正反两面照片')
  },
  // 校验是否填写完整
  validata (isBase, callback) {
    // 发布和新增节点的保存都会调用该方法 isBase true表示是发布模板页面调用 否是创建节点保存
    if (
      isBase
        ? this.WxValidate.checkForm(this.data.tempData)
        : this.WxValidate.checkForm(this.data.currentStepsDate)
    ) {
      callback(true)
    } else {
      let errList = {}
      this.WxValidate.errorList.forEach((item) => {
        errList[item.param] = item.msg
      })
      console.log('errList: ', errList)
      this.setData({
        errList,
      })
      callback(false)
    }
  },
  //#endregion

  //#region 获取当前页面数据
  // 获取当前页面数据
  getPostData () {
    const tempData = JSON.parse(JSON.stringify(this.data.tempData))
    let allDaysData = this.selectComponent('#k-swiper').getAllDaysData()
    console.log('swper组件内数据: ', allDaysData);
    allDaysData.forEach((day, i) => {
      day.forEach((place, pIndex) => {
        place.nodeSequence = pIndex
      })
    })
    // console.log('pageData: ', pageData)
    const postData = {
      id: tempData.id,
      userId: wx.getStorageSync('userId'),
      templateName: tempData.templateName,
      expense: Number(tempData.expense || null),
      // playTime:  tempData.playTime?.join(),
      // cityName:  tempData.cityName ?  tempData.cityName[0] : null,
      cityName: tempData.cityName ? tempData.cityName : null,
      mainImageUrl: tempData.mainImageUrl
        ? tempData.mainImageUrl[0]
        : tempData.mainImageUrl,
      // templatenodeDTOList:  tempStepList,
      nodeDTOList: allDaysData,
    }
    return postData
  },
  //#endregion
  //#region 提交整个攻略模板
  // 提交整个攻略模板
  onAddTempSubmit () {
    const postData = this.getPostData()
    console.log('getPostData数据: ', postData)
    this.initValidate(true)
    this.validata(true, (valid) => {
      if (valid) {
        let allDaysData = this.selectComponent('#k-swiper').getAllDaysData()
        let lessDayIndexs = []
        allDaysData.forEach((item, i) => {
          if (item.length < 2) {
            lessDayIndexs.push(i + 1)
          }
        })
        console.log('lessDayIndexs: ', lessDayIndexs);
        // return
        if (lessDayIndexs.length) {
          getApp().globalData.Message.info({
            context: this,
            offset: [90, 90],
            duration: 3000,
            theme: 'warning',
            // single: false, // 打开注释体验多个消息叠加效果
            content: `第${lessDayIndexs.join(' , ')}天最少需要两个景点`,
          })
          return
        }

        // return
        const apiFuncion = this.data.isCopy ? copyTemplate : this.data.isFormEdit ? editTemplate : addTemplate
        console.log('apiFuncion: ', apiFuncion);
        apiFuncion(this.data.isCopy ? postData.id : postData).then((res) => {
          if (res.success) {
            wx.showModal({
              title: '提示',
              content: this.data.isCopy ? '复制成功' : this.data.isFormEdit ? '更新成功' : '发布成功',
              showCancel: false,
              complete: (res) => {
                let pages = getCurrentPages();
                let prevPage = pages[pages.length - 2]; // 获取上一个页面实例对象
                let delta = pages.length - prevPage.index - 1; // 计算需要返回的页面数

                if (res.confirm) {
                  // deleteDraft().then()
                  wx.navigateBack({
                    delta,
                    success: function () {
                      prevPage.onLoad({ id: postData.id, userId: wx.getStorageSync('userId') }); // 执行上一页的onLoad函数
                    }
                  })
                }
              },
            })
          }
        })
      }
    })
  },
  //#endregion
})
