import { submitFeedBack } from '@/api/feedback/feedback'
Page({
  data: {
    disabled: true
  },
  onLoad (options) {

  },
  textareaInput (e) {
    if (e.detail.value) {
      this.setData({
        disabled: false
      })
    } else {
      this.setData({
        disabled: true
      })
    }
    this.setData({
      content: e.detail.value.trim()
    })

  },
  contactInput (e) {
    this.setData({
      contact: e.detail.value.trim()
    })
  },
  submit () {
    let { content, contact } = this.data
    submitFeedBack({ content, contact }).then(res => {
      if (res.success) {
        wx.showModal({
          title: '提交成功',
          content: '感谢反馈!',
          showCancel: false,
          complete: (res) => {
            if (res.confirm) {
              wx.navigateBack()
            }
          }
        })
      }
    })
  }
});