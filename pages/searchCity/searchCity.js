import { getTemplateRank } from '@/api/template/template'
Page({

  data: {
    value: '',
    rankList: [],
    showRankList: true,
    searchData: {
      current: 1,
      size: 10,
      cityName: "",
    }
  },

  onLoad () {
    getTemplateRank(4).then(res => {
      if (res.success) {
        this.setData({
          rankList: res.data
        })
      }
    })
  },
  // 输入框输入
  onChangeValue ({ detail }) {
    this.setData({
      value: detail.value
    })
  },
  // 输入框点击清空按钮
  onInputClear () {
    this.setData({
      value: '',
      showRankList: true
    })
  },
  // 点击nav返回
  handleBack () {
    console.log('go back');
  },
  // 排行榜点击
  onRankTextTap (e) {
    let searchData = { ...this.data.searchData, cityName: e.target.dataset.value }
    this.setData({
      value: e.target.dataset.value,
      searchData,
      showRankList: false,
      needQuery: true
    })
  },
  onSearchSubmit ({ detail }) {
    if (detail) {
      this.setData({
        showRankList: false,
        needQuery: false
      })
      let searchData = { ...this.data.searchData, cityName: detail.value }
      console.log('searchData: ', searchData);
      setTimeout(() => {
        console.log('this.selectComponent(', this.selectComponent('#search'));
        this.selectComponent('#search')?.querySearch(searchData)

      });


    }
  }
})