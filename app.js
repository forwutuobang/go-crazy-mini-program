import Message from 'tdesign-miniprogram/message/index'
import { isHadLogin } from '@/api/login/login'

App({
  onLaunch () {
    // wx.loadFontFace({
    //   family: 'myfont',
    //   global: true,
    //   source: 'url("/image/AGENCYR.TTF")',  //此处需替换为真实字体地址
    //   success (res) {
    //     console.log(res.status)
    //   },
    //   fail: function (res) {
    //     console.log(res.status)
    //   },
    //   complete: function (res) {
    //     console.log(res.status)
    //   }
    // });
    // 判断当前运行环境，设置不同的 API 网址
    // console.log('wx.getAccountInfoSync().miniProgram.envVersion: ', wx.getAccountInfoSync().miniProgram.envVersion);
    // if (wx.getAccountInfoSync().miniProgram.envVersion === "develop") {
    //   this.globalData.apiUrl = "https://followapi.prudencemed.com";
    // } else {
    //   this.globalData.apiUrl = 'https://followapi.prudencemed.com';
    // }
    this.examTokenIsExpried()
    // wx.navigateTo({
    //   url: "pages/myTest/test",
    //   complete: (res) => {
    //     console.log('res ', res)
    //   }
    // },
    // )
    /* "requiredPrivateInfos": ["getLocation"],
  "permission": {
    "scope.userLocation": {
      "desc": "你的位置信息将用于小程序位置接口的效果展示"
    }
  } */

  },
  globalData: {
    apiUrl: 'https://www.gocrazy.icu/api',
    loadingCount: 0,
    Message,
  },
  examIsLogined (callback) {
    wx.login({
      success: (res) => {
        if (res && res.code) {
          isHadLogin(res.code).then((_res) => {
            if (_res.success) {
              callback(true, _res)
            } else {
              callback(false)
            }
          })
        }
      },
    })
  },
  examTokenIsExpried () {
    // 先判断是非有token
    if (!wx.getStorageSync('authToken')) {
      // 判断是非token过期
      // 获取当前时间
      const currentTime = new Date().getTime()
      console.log('currentTime: ', currentTime);
      const expirationTime = wx.getStorageSync('authTokenExpirationTime') || 0
      console.log('expirationTime: ', expirationTime);
      if (currentTime >= expirationTime) {
        this.examIsLogined((isLogin, loginRes) => {
          if (isLogin) {
            this.setTokenOthers(loginRes)
            wx.reLaunch({
              url: '/pages/index/index',
            })
          } else {
            wx.showModal({
              title: '还没有账号,先注册一个吧！',
              showCancel: false,
              success: (res) => {
                if (res.confirm) {
                  wx.reLaunch({
                    url: '/pages/login/login',
                  })
                }
              },
            })
          }
        })
      }
    } else {
      const currentTime = new Date().getTime()
      console.log('currentTime: ', currentTime);
      const expirationTime = wx.getStorageSync('authTokenExpirationTime') || 0
      console.log('expirationTime: ', expirationTime);
      if (currentTime >= expirationTime) {
        this.examIsLogined((isLogin, loginRes) => {
          this.setTokenOthers(loginRes)
          wx.reLaunch({
            url: '/pages/index/index',
          })
        })
      }
    }
    // else {
    //   // 是否注册过
    //   this.examIsLogined((isLogin, loginRes) => {
    //     // 如果注册过 但是可能清除缓存没有token
    //     if (isLogin) {
    //       this.setTokenOthers(loginRes)
    //       wx.reLaunch({
    //         url: '/pages/index/index',
    //       })
    //     } else {
    //       wx.showModal({
    //         title: '还没有账号,先注册一个吧！',
    //         showCancel: false,
    //         success: (res) => {
    //           if (res.confirm) {
    //             wx.reLaunch({
    //               url: '/pages/login/login',
    //             })
    //           }
    //         },
    //       })
    //     }
    //   })

    // }

  },
  setTokenOthers (loginRes) {
    wx.setStorageSync('authToken', loginRes.data.authToken)
    wx.setStorageSync('authTokenExpirationTime', loginRes.data.authTokenExpirationTime)
    wx.setStorageSync('openid', loginRes.data.openid)
    wx.setStorageSync('userId', loginRes.data.userId)
  }
})
