// components/nav-bar/nav-bar.js
Component({
  lifetimes: {
    attached () {
      this.initRect()
    }
  },
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    navHeight: '',
    menuButtonInfo: {},
    searchMarginTop: 0, // 搜索框上边距
    searchWidth: 0, // 搜索框宽度
    searchHeight: 0 // 搜索框高度
  },
  /**
   * 组件的方法列表
   */
  methods: {
    initRect () {
      this.setData({
        menuButtonInfo: wx.getMenuButtonBoundingClientRect()
      })
      const { top, width, height, right } = this.data.menuButtonInfo
      wx.getSystemInfo({
        success: (res) => {
          const { statusBarHeight } = res
          const margin = top - statusBarHeight
          this.setData({
            navHeight: (height + statusBarHeight + (margin * 2)) + 10,
            searchMarginTop: statusBarHeight + margin, // 状态栏 + 胶囊按钮边距
            searchHeight: height,  // 与胶囊按钮同高
            searchWidth: right - width // 胶囊按钮右边坐标 - 胶囊按钮宽度 = 按钮左边可使用宽度
          })
          this.triggerEvent('navHeight', height)
        },
      })

    },
    onSearchTap () {
      this.triggerEvent('onSearchTap')
    }
  }
})