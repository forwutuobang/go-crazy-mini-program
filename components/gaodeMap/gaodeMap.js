// var amapFile = require('@/utils/amap-wx.130.js');
// import { Config } from '@/libs/mapConf.js';
import { copyTemplate } from '@/api/template/template'
Component({
  lifetimes: {
    attached: function () {
      this.initMap()
    }
  },
  pageLifetimes: {
    show: function () {
      // 页面被展示
    },
    hide: function () {
      // 页面被隐藏
    },
    resize: function (size) {
      // 页面尺寸变化
    }
  },
  properties: {
    tempData: {
      type: Object,
      value: () => {
        return {}
      }
    },
    moveTop: {
      type: Number,
      value: 0
    },
    enableScroll: {
      type: Boolean,
      value: true
    },
    bottomAreaSize: {
      type: String,
      value: '0'
    }
  },
  data: {
    markers: [],
    latitude: '',
    longitude: '',
    polyline: [],
    popupVisble: false,
    placeNodeDetail: {},
    mapCtx: '',
    checkValueList: [],
    scale: ''
  },

  methods: {
    initMap () {
      const tempData = this.data.tempData
      const placeInfo = this.data.tempData.nodeDTOList
      console.log(tempData);
      this.mapCtx = wx.createMapContext('map', this);
      // 获取每个位置坐标list
      let polylineList = this.getAllLatitudeAndLongitude(placeInfo)
      // 设置每个坐标位置标注及样式
      this.setMakers(placeInfo)

      this.setPolyline(placeInfo)
      // 获取计算中心位置的经纬度
      let latitude = this.calculateCenter(polylineList).latitude
      let longitude = this.calculateCenter(polylineList).longitude
      this.setData({
        latitude,
        longitude,
        tempData,
        userId: wx.getStorageSync('userId')
      })

      setTimeout(() => {
        this.setAllPointScale(placeInfo)
        // this.setAllPointScale()
        // setTimeout(() => {

        //   this.mapCtx.moveAlong({
        //     markerId: tempData.templateNodeRespDTOList[tempData.templateNodeRespDTOList.length - 1].id,
        //     path: polylineList,
        //     duration: 2000,
        //     autoRotate: false
        //   })
        // }, 1000);
      }, 300);

    },
    // 设置两个或者多个点画线
    setPolyline (placeInfo) {
      let copyPlaceInfo = JSON.parse(JSON.stringify(placeInfo))
      let baseConfig = {
        points: [],
        color: "#409EFF",// 深色
        width: 8,
        arrowLine: true,
        borderColor: '#000000',
        borderWidth: 1,
        level: 'abovelabels',
        // colorList: this.getMapColorList(),
        // segmentText: [
        //   {
        //     startIndex: 0,
        //     endIndex: 1,
        //     name: '123'
        //   },
        //   {
        //     startIndex: 1,
        //     endIndex: 2,
        //     name: '456'
        //   },
        //   {
        //     startIndex: 2,
        //     endIndex: 3,
        //     name: '456'
        //   },
        // ],
        // textStyle: {
        //   textColor: '#ff0000',
        //   strokeColor: '#ff0000',
        //   fontSize: 20
        // },
      }
      let highlightObj = {}
      let normalObj = {}

      let { checkValueList } = this.data
      let color = "#409EFF"
      if (checkValueList.length !== copyPlaceInfo.length) {// 如果不是全部选择天数
        const polylineConfigList = []
        checkValueList.forEach(checkValue => {
          placeInfo[checkValue].forEach(dayItem => {
            polylineConfigList.push({
              latitude: parseFloat(dayItem.latitude), longitude: parseFloat(dayItem.longitude)
            })
          })
          copyPlaceInfo.splice(checkValue, 1)
        })
        highlightObj = { ...baseConfig, points: polylineConfigList, borderWidth: 2 }
        color = "#A6CFFE"

      } else { // 天数全部选中直接设置全部坐标返回中心点
        this.setAllPointScale(placeInfo)
      }

      let polylineList = this.getAllLatitudeAndLongitude(copyPlaceInfo, false)
      normalObj = { ...baseConfig, points: polylineList, color }
      this.setData({
        polyline: [normalObj, highlightObj]
      })

      return
    },


    // 设置marker点
    setMakers (placeInfo) {
      let copyPlaceInfo = JSON.parse(JSON.stringify(placeInfo));
      let { checkValueList } = this.data;
      checkValueList = checkValueList.slice().sort((a, b) => a - b);
      let checkedMakerList = [];

      if (checkValueList.length) {
        checkValueList.forEach(checkValue => {
          placeInfo[checkValue].forEach(dayItem => {
            checkedMakerList.push(dayItem)
          });
          copyPlaceInfo.splice(checkValue, 1)
        });
        // copyPlaceInfo.splice(...checkValueList.map(value => [value, 1]));
        // console.log('checkValueList.map(value => [value, 1]): ', checkValueList.map(value => [value, 1]));
      }
      /*  假设 checkValueList 包含[1, 3, 5]，那么 checkValueList.map(value => [value, 1]) 
          将返回[[1, 1], [3, 1], [5, 1]]。使用扩展运算符 ...调用 splice 时，
          实际上是执行了以下操作：
          splice (1, 1) 会移除 copyPlaceInfo 中索引为1的元素。
          splice (3, 1) 会移除 copyPlaceInfo 中索引为3的元素。
          splice (5, 1) 会移除 copyPlaceInfo 中索引为5的元素
       */
      // checkedMakerList = checkedMakerList.sort((a, b) => a.day - b.day)
      const createMarkerOptions = (item, index, isHighlight) => ({
        id: item.id,
        latitude: item.latitude,
        longitude: item.longitude,
        iconPath: getIconPath(index, isHighlight ? checkedMakerList.length : copyPlaceInfo.length),
        width: 23,
        height: 33,
        zIndex: isHighlight ? 100 : 1,
        // collision: "poi,marker",
        // 'collision-relation': "together",
        alpha: 1,
        callout: {
          content: `${index + 1}-${item.address}`,
          display: 'ALWAYS',
          color: '#FFFFFF',
          bgColor: isHighlight ? '#8F86ED' : '#292C34',
          padding: isHighlight ? 14 : 8,
          borderRadius: 10,
          borderWidth: 3,
          borderColor: isHighlight ? '#1E2227' : '',
          fontSize: 12,
        },
      });

      const getIconPath = (index, totalLength) => {
        if (index === 0) return '../../image/mapicon_navi_s.png';
        if (index + 1 === totalLength) return '../../image/mapicon_navi_e.png';
        return '../../image/mapicon_RoutePoint.png';
      };

      const markerList = [
        ...checkedMakerList.map((item, i) => createMarkerOptions(item, i, true)),
        ...this.getAllLatitudeAndLongitude(copyPlaceInfo).map((item, i) => createMarkerOptions(item, i, false)),
      ];

      console.log('markerList: ', markerList);
      this.setData({ markers: markerList });
    },
    // 获取数据中的所有经纬度list
    getAllLatitudeAndLongitude (placeInfo, needBaseInfo = true) {
      const coordinates = [];
      // 遍历每一项
      placeInfo.forEach(day => {
        // 遍历每一天的条目
        day.forEach(item => {
          // 提取经纬度信息
          // 将经纬度信息添加到新数组
          if (needBaseInfo) {
            coordinates.push({
              ...item,
              latitude: parseFloat(item.latitude),
              longitude: parseFloat(item.longitude),
            });
          } else {
            coordinates.push({
              latitude: parseFloat(item.latitude),
              longitude: parseFloat(item.longitude),
            });
          }

        });
      });
      return coordinates
    },

    // 设置缩放最中心点 使用mapctx方法 能设置padding 目前使用这个
    setAllPointScale (placeInfo) {
      const includePoints = this.getAllLatitudeAndLongitude(placeInfo)
      this.mapCtx.includePoints({
        padding: [150, 90, 200, 90],
        points: includePoints //放入所有坐标轴的数组   并引用此方法
      })
    },
    // 设置缩放最中心点 使用map传值属性includePoints 不能设置padding 
    setIncludePoints (placeInfo) {
      const polylineList = this.getAllLatitudeAndLongitude(placeInfo)
      this.setData({
        includePoints: polylineList
      })
      // setTimeout(() => {
      //   this.mapCtx.getScale({
      //     success: (res) => {
      //       console.log('res: ', res);
      //       this.setData({
      //         scale: res.scale - 0.1
      //       })
      //     }
      //   })
      // }, 1000);
    },
    // 设置多个路径的彩虹线
    getMapColorList () {
      return ['#bac8ff', '#ffc9c9', '#96f2d7']
    },
    // 计算所有坐标点的中心点
    calculateCenter (points) {
      let totalLongitude = 0;
      let totalLatitude = 0;
      points.forEach(point => {
        totalLongitude += point.longitude;
        totalLatitude += point.latitude;
      });
      const centerLongitude = totalLongitude / points.length;
      const centerLatitude = totalLatitude / points.length;

      return { longitude: centerLongitude, latitude: centerLatitude };
    },
    makerTap (e) {
      console.log('makerTap: ', e);
      this.showNodeDetail(e)
    },
    calloutTap (e) {
      console.log('e: ', e);
      this.showNodeDetail(e)
    },
    showNodeDetail (e) {
      if (!this.data.enableScroll) return
      let points = this.getAllLatitudeAndLongitude(this.data.tempData.nodeDTOList)

      const nodeItem = points.find(item => item.id === e.detail.markerId)
      this.setData({
        placeNodeDetail: nodeItem,
        popupVisble: true
      })
    },
    onClose () {
      this.setData({
        popupVisble: false
      })
    },
    onVisibleChange () {
      this.setData({
        popupVisble: false
      })
    },
    changeMarkerColor (data, i) {
      var markers = [];
      for (var j = 0; j < data.length; j++) {
        if (j == i) {
          data[j].iconPath = "../../image/marker_checked.png"; //如：..­/..­/img/marker_checked.png
        } else {
          data[j].iconPath = "../../image/marker.png"; //如：..­/..­/img/marker.png
        }
        markers.push(data[j]);
      }
      this.setData({
        markers: markers
      });
    },
    navigationLoaction () {
      wx.openLocation({
        latitude: Number(this.data.placeNodeDetail.latitude),
        longitude: Number(this.data.placeNodeDetail.longitude),
        scale: 18,
        name: this.data.placeNodeDetail.address
      })
    },
    // 返回轨迹中心点
    onBackTap () {
      this.setAllPointScale(this.data.tempData.nodeDTOList)
      // this.mapCtx.moveToLocation({
      //   longitude: this.calculateCenter(this.getAllLatitudeAndLongitude(this.data.tempData)).longitude,
      //   latitude: this.calculateCenter(this.getAllLatitudeAndLongitude(this.data.tempData)).latitude,
      //   complete: (res) => {
      //     console.log(res);
      //   }
      // })
    },
    dayTap (e) {
      let dataset = e.target.dataset
      let { checkValueList } = this.data

      let finded = checkValueList.findIndex(item => item === dataset.index)

      if (finded === -1) {
        checkValueList.push(dataset.index)
      } else {
        checkValueList.splice(finded, 1)
      }

      this.setData({
        checkValueList
      })

      wx.nextTick(() => {
        let placeInfo = JSON.parse(JSON.stringify(this.data.tempData.nodeDTOList))

        // let polyline = this.setPolyline(tempData)
        this.setPolyline(placeInfo)
        this.setMakers(placeInfo)
        this.setData({
          markers: this.data.markers
        })
        if (checkValueList.length) {
          let nodeDTOList = []
          checkValueList.forEach(dayIndex => {
            nodeDTOList.push(placeInfo[dayIndex])
          })
          placeInfo = nodeDTOList
        }
        this.setAllPointScale(placeInfo)
        // this.setData({
        //   polyline
        // })
      })

    },
    onEditTap () {

      wx.navigateTo({
        url: `/pages/createTemp/createTemp?templateId=${this.data.tempData.id}`,
      })
    },
    onReviewTap () {
      wx.navigateTo({
        url: `/pages/createTemp/createTemp?templateId=${this.data.tempData.id}&isReadOnly=true`,
      })
    },
    onCopyTap () {
      wx.showModal({
        title: '复制模版',
        content: '是否复制该模版,复制将自动发布',
        cancelText: '否',
        confirmText: '是',
        complete: (res) => {
          if (res.cancel) { }
          if (res.confirm) {
            copyTemplate(this.data.tempData.id).then(res => {
              if (res.success) {
                wx.showModal({
                  title: '复制成功',
                  content: '是否现在编辑模版',
                  cancelText: '否',
                  confirmText: '是',
                  complete: (res) => {
                    if (res.cancel) { }
                    if (res.confirm) {
                      wx.navigateTo({
                        url: `/pages/createTemp/createTemp?templateId=${this.data.tempData.id}`,
                      })
                    }
                  }
                })
              }
            })
          }
        }
      })
    }
  }
})