import { getTemplateList, likeTravelTemplate, templateRemove, templateCollectRemove } from '@/api/template/template'
import { NIGHTLIST, DAYLIST } from '@/utils/localData'
let scrollThreshold = 700
Component({
  lifetimes: {
    created () { },
    async ready () {
      const currentTime = new Date().getTime()
      const expirationTime = wx.getStorageSync('authTokenExpirationTime')

      if (wx.getStorageSync('authToken') && currentTime <= expirationTime && this.data.needQuery) {
        await this.getTempItemRect()
        await this.querySearch(this.properties.searchData)
      }
      wx.getSystemInfo({
        success (res) {
          scrollThreshold = (res?.windowHeight - 150) * 0.5
        }
      })
    },
  },
  properties: {
    searchData: {
      type: Object,
      value: () => {
        return {
          current: 1,
          size: 10,
          cityName: '',
        }
      },
    },
    needQuery: {
      type: Boolean,
      value: true,
    },
    searchFunction: {
      type: Function,
      value: getTemplateList,
    },
    useTag: {
      typeof: String,
      value: ''
    },
    deteleFuction: {
      type: Function,
      value: null
    }
  },
  data: {
    loadingVisible: true,
    templateList: [],
    imgWidth: null,
    imgHeight: null,
    imageSrc: 'https://tdesign.gtimg.com/mobile/demos/image1.jpeg',
    pics: [
      'https://tdesign.gtimg.com/mobile/demos/avatar1.png',
      'https://tdesign.gtimg.com/mobile/demos/avatar2.png',
      'https://tdesign.gtimg.com/mobile/demos/avatar3.png',
      'https://tdesign.gtimg.com/mobile/demos/avatar4.png',
      'https://tdesign.gtimg.com/mobile/demos/avatar5.png',
      'https://tdesign.gtimg.com/mobile/demos/avatar1.png',
    ],
    iconNames: ['heart', 'heart-filled'],
    enable: false,
    loadingProps: {
      size: '20px',
    },
    rowCol1: [
      { width: '330rpx', height: '280rpx', borderRadius: '24rpx' },
      { width: '90%' },
      { width: '50%' },
      [{ size: '30px', type: 'circle' }, { width: '30%' }],
    ],
    scrollTop: 0,
    pullDisabled: false,
    isEdit: false,
    checkList: [],
    itemDivWidth: 0,
    itemDivOpacity: 0,
    leftAllH: 0,
    rightAllH: 0,
  },

  /**
   * 组件的方法列表
   */
  methods: {
    async getTempItemRect () {
      return new Promise((reslove, reject) => {
        let query = this.createSelectorQuery()
        query.select('.container').boundingClientRect((rect) => {
          if (!rect) {
            setTimeout(() => {
              this.getTempItemRect()
            }, 200);
          } else {
            this.setData({
              rect
            })
            reslove()
          }
        }).exec()
      })
    },
    // setImgRect (width) {
    //   console.log('width: ', width);
    //   // 在自定义组件中 使用this.createSelectorQuery
    //   // 获取每个攻略布局的宽度，动态设置图片的宽高
    //   let templateList = this.data.templateList
    //   templateList.forEach(item => {
    //     item.imgWidth = width * 0.48
    //     if (!item.mainImageViewType) {
    //       item.imgHeight = item.imgWidth * 1.2
    //     } else {
    //       item.imgHeight = item.imgWidth * 0.8
    //     }
    //   })
    //   this.setData({
    //     templateList
    //   })
    // },
    // 获取攻略列表
    async querySearch (data, callback, showLoading = true, needClear) {
      let { leftAllH, rightAllH } = this.data
      this.data.searchFunction(data, showLoading).then((res) => {
        if (res.success) {
          if (res.data.records && res.data.records.length) {
            res.data.records.forEach((item, index) => {
              item.width = this.data.rect.width * 0.48 - 4
              if (item.mainImageViewType) {
                item.isVertical = true
                item.height = item.width * 1.4
              } else {
                item.isVertical = false
                item.height = item.width * 0.8
              }

              item.expense = String(item.expense)

              item.playTime && item.playTime?.split(',').forEach((_item, i) => {
                if (i === 0) {
                  item.playTimeText = DAYLIST[_item - 1]?.label
                }
                if (i === 1) {
                  Number(_item) === 0 ? '' : (item.playTimeText += ',' + NIGHTLIST[_item].label)
                }
              })

            })
            const tempList = res.data.records
            tempList.forEach((item, i) => {
              if (i === 0) {
                item.left = true
                leftAllH += item.height
              } else {
                if (leftAllH > rightAllH || leftAllH === rightAllH) {
                  item.right = true
                  rightAllH += item.height
                } else {
                  item.left = true
                  leftAllH += item.height
                }
              }
            })
            if (needClear) {
              this.data.templateList = []
              this.setData({
                scrollTop: 0
              })
            }
            this.setData({
              templateList: [...this.data.templateList, ...tempList],
              enable: false,
            })
            console.log('templateList: ', this.data.templateList);

            // this.getTempItemRect()

            callback && callback()
            if (data.current > 1) return
            const animation = wx.createAnimation({
              duration: 700,
              timingFunction: 'ease-in-out',
            })
            animation.opacity(0.01).step({ duration: 100 }).opacity(1).step()
            this.setData({
              tempAnimation: animation.export(),
            })
          }
          else {
            if (data.current === 1)
              this.setData({
                templateList: null
              })
          }
        }
      })
    },
    // 点赞点击
    onLikesTap (e) {
      const index = this.isCurrentTarget(e)
      let tapTemplate = this.getTemplateItem(index)
      likeTravelTemplate({
        templateId: tapTemplate.id,
        userId: tapTemplate.userId,
      }).then((res) => {
        if (res.success) {
          tapTemplate.liked = !tapTemplate.liked
          if (tapTemplate.liked) {
            tapTemplate.likesCount += 1
          } else {
            tapTemplate.likesCount -= 1
          }
          this.setData({
            [`templateList[${index}]`]: tapTemplate,
          })
        }
      })
    },
    // 单个模板点击-增加浏览量
    onTemplateTap (e) {
      if (this.data.isEdit) {
        let index = this.isCurrentTarget(e)
        let id = this.isCurrentTarget(e, 'id')
        this.setCheck(index, id)
      } else {
        const id = this.isCurrentTarget(e, 'id')
        wx.navigateTo({
          url: `/pages/tempDetail/tempDetail?id=${id}`,
        })
      }
    },
    isCurrentTarget (e, name = 'index') {
      return e.currentTarget.dataset[name]
    },
    getTemplateItem (index) {
      return this.data.templateList[index]
    },
    onRefresh () {
      let query = { ...this.data.searchData, current: 1 }
      this.querySearch(query, () => {
        this.setData({ enable: true },
        )
      }, true, true
      )
    },
    radioChange (e) {
      console.log('radioChange', e.detail.value)
    },
    showCheckBtn () {
      this.setData({
        isEdit: true
      })
    },
    setCheck (index, id) {
      let checkList = this.data.checkList
      // 如果checkList有当前点击id 就删除 否则添加
      const findedIndex = checkList.findIndex(item => item === id)
      if (findedIndex !== -1) {
        checkList.splice(findedIndex, 1)
      } else {
        checkList.push(id)
      }
      // 设置点击元素的isChecked
      this.data.templateList[index].isChecked = !this.data.templateList[index].isChecked
      this.setData({
        templateList: this.data.templateList,
        checkList
      })

    },
    onCancelEditTap () {
      this.data.templateList.forEach(item => {
        item.isChecked = false
      })
      this.setData({
        isEdit: false,
        checkList: [],
        templateList: this.data.templateList
      })
      // const animation = wx.createAnimation({
      //    duration: 300,
      //    timingFunction: 'ease-in-out',
      // })
      // animation.height(height).opacity(isShow ? 1 : 0).step()
      // this.setData({
      //    animation: animation.export(),
      // })
    },
    onDeleteTap () {
      if (this.data.checkList.length) {
        const postData = {
          "templateIdList": this.data.checkList,
          "userId": wx.getStorageSync('userId')
        }
        const apiFunction = this.properties.deteleFuction
        apiFunction(postData).then(res => {
          if (res.success) {
            getApp().globalData.Message.success({
              context: this,
              offset: [50, 50],
              duration: 3000,
              theme: 'info',
              // single: false, // 打开注释体验多个消息叠加效果
              content: `删除成功`,
            })
            this.querySearch(this.data.searchData, null, true, true)
          }
        })
      }
    },
    onScrollMoveing ({ detail }) {
      this.data.scrollTop = detail.scrollTop
      const scrollTop = Math.floor(detail.scrollTop)
      // console.log('scrollTop: ', scrollTop);
      let current = this.properties.searchData.current
      let scrollPageNumber = Math.floor(scrollTop / scrollThreshold + 1)
      // console.log('scrollPageNumber: ', scrollPageNumber);
      if (current < scrollPageNumber) {
        this.properties.searchData.current = scrollPageNumber
        if (scrollTop && scrollTop > scrollThreshold * (current - 1)) {
          this.querySearch(this.properties.searchData, null, false)
        }
      }

    },
  },
})
