Component({
  lifetimes: {
    created () {

    },
    attached () {
      this.initSwiperStyle()
      console.log('this.data.swiperList: ', this.data.swiperList);
      if (!this.data.isReadOnly) {
        if (!this.data.swiperList || this.data.swiperList.length === 0) {
          this.data.swiperList = [[]]
        } else {
          this.data.swiperList.push([[]])
        }
        this.setData({
          swiperList: this.data.swiperList
        })
      }
      console.log('this.data.swiperList: ', this.data.swiperList);


    },
  },
  observers: {
    swiperList: function (value) {
      console.log('observers: ', value);
      let lengthNumber = 180
      let height = 0
      if (value.length) {
        value.forEach(function (item) {
          if (item.length > lengthNumber) {
            lengthNumber = item.length
            height = item.length * 130 + 80
          }
        })
        this.setData({
          lengthNumber: calcHeight
        })
      }
    },
  },
  properties: {
    swiperList: {
      type: Array,
      value: [],

    },
    isReadOnly: {
      type: Boolean,
      value: false
    }
  },
  data: {
    current: 0,
    title: '',
    editDayItemIndex: null,
    calcHeight: 400
  },

  methods: {
    change (e) {
      let current = e.detail.current
      this.setData({
        current,
      })
    },
    setSwiperListItem (data) {
      console.log('setSwiperListItemdata: ', data);
      // let editDayItemIndex = this.data.editDayItemIndex
      let { swiperList, current, editDayItemIndex } = this.data
      if (this.data.editDayItemIndex === null) {
        if (swiperList[current] && swiperList[current][0] && !swiperList[current][0].address) {
          swiperList[current][0] = data
        } else {
          swiperList[current].push(data)

        }
        if (!swiperList[current + 1]?.length) {
          swiperList[current + 1] = []
          swiperList[current + 1][0] = []
        }
      } else {
        swiperList[current][editDayItemIndex] = data
      }

      console.log('swiperList: ', swiperList);

      this.setData({
        swiperList
      })

    },
    initSwiperStyle () {
      const myAnimation = () => {
        return wx.createAnimation({
          duration: 500,
          timingFunction: 'ease',
        })
      }

      const animation1 = myAnimation()
      const animation2 = myAnimation()
      const onShowAnimation = myAnimation()
      animation1.opacity(1).scale(1).step()
      animation2.opacity(0.5).scale(0.9).step()
      onShowAnimation.opacity(1).rotateY(0).rotateX(0).step()
      this.setData({
        stretchAnimation: animation1.export(),
        shrinkAnimation: animation2.export(),
        onShowAnimation: onShowAnimation.export()
      })
    },
    // 点击添加新一天第一个景点
    addFirstDay (e) {
      this.data.editDayItemIndex = null

      let dataset = e.currentTarget.dataset
      this.triggerEvent('addDateOrPlace', { dayNumber: dataset.index, dayPlaceNumber: 0 })
    },
    // 某天的某个景点点击
    onStepsTap (e) {
      let dataset = e.currentTarget.dataset
      console.log('dataset: ', dataset);

      this.triggerEvent('onStepsPlaceTap', { data: dataset.stepitem, dayPlaceNumber: dataset.index })
      this.data.editDayItemIndex = dataset.index
    },
    editPlace (e) {

    },
    // 点击新增某一天的新增景点
    addPlace (e) {
      this.data.editDayItemIndex = null

      let dataset = e.target.dataset
      let dayPlaceNumber = this.data.swiperList[dataset.index].length
      this.triggerEvent('addDateOrPlace', { dayNumber: dataset.index, dayPlaceNumber })
    },
    deleteStepItem (e) {
      let dataset = e.currentTarget.dataset
      let swiperList = this.data.swiperList

      wx.showModal({
        title: '删除景点',
        content: `景点:${swiperList[dataset.index][dataset.stepindex].address}`,
        complete: (res) => {
          if (res.confirm) {
            swiperList[dataset.index].splice(dataset.stepindex, 1)
            this.setData({
              swiperList
            })
          }
          if (res.cancel) {

          }
        }
      })

    },
    getAllDaysData () {
      let data = JSON.parse(JSON.stringify(this.data.swiperList))
      data.pop()
      return data
    },
    getSingleDayData () {
      return this.data.swiperList[this.data.current]
    },
    calcAllPrice () {
      console.log(this.data.swiperList)
      let { swiperList } = this.data
      let dayAllExpense = 0
      swiperList.forEach(day => {
        day.forEach(place => {
          if (place.address) {
            console.log('place: ', place);
            dayAllExpense = dayAllExpense + place.nodeExpense
          }
        })
      })
      console.log('swiperList: ', swiperList);

    },
    calculateDailyExpenses () {
      let data = this.data.swiperList
      console.log('data: ', data);
      let totalExpense = 0;

      // 遍历外层数组中的每一天
      data.forEach(day => {
        console.log('day: ', day);
        // 遍历内层数组中的每个行程节点
        day.forEach(node => {
          // 累加每个节点的花费
          if (node.nodeExpense === null || node.nodeExpense === undefined) {
            node.nodeExpense = 0
          }
          totalExpense += Number(node.nodeExpense);
        });
        console.log('totalExpense: ', totalExpense);
      });

      return totalExpense;
    }

  },
});
