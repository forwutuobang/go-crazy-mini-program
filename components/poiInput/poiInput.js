var amapFile = require('@/utils/amap-wx.130.js')
import { Config } from '@/libs/mapConf.js'
let myAmapFun = new amapFile.AMapWX({ key: Config.key })

Component({
  properties: {
    city: {
      type: String,
      value: '',
    },
    inputValue: {
      type: String,
      value: '',
    },
    citylimit: {
      //仅返回指定城市数据。
      type: Boolean,
      value: true,
    },
    location: {
      //经纬度坐标。设置该参数会在此 location 附近优先返回关键词信息。格式：'经度,纬度'
      type: String,
      value: '',
    },
    showErrText: {
      type: Boolean,
      value: false,
    },
    errText: {
      type: String,
      value: '',
    },
    isReadOnly: {
      type: Boolean,
      value: false
    }
  },
  data: {
    nearbyAddressList: [],
    opacityValue: 0,
    addressItemInfo: {},
  },
  lifetimes: {
    attached () {
      this.setRect()
    },
  },
  methods: {
    // 获取附近位置
    getNearby (value) {
      let that = this
      console.log('that: ', that);
      myAmapFun.getInputtips({
        keywords: value,
        location: that.data.location,
        city: that.data.city,
        citylimit: that.data.citylimit,
        success: function (res) {
          // 判断是否有inputValue ,有可能输入框内容改变过快,没内容后还在请求,会导致把请求结果重新添加到nearbyAddressList
          if (res && res.tips && that.data.inputValue) {
            res.tips = res.tips.filter((item) => {
              return item.location?.length > 0
            })

            that.setData({
              nearbyAddressList: res.tips.slice(0, 5),
            })
            that.handelAnimation('auto')
          }
        },
        fail: function (err) {
          console.log('err', err)
        },
      })
    },
    // 输入框点击如果有数据 查询输入框数据
    onInputFocus ({ detail }) {
      if (detail.value) {
        this.getNearby(detail.value)
      }
    },
    // 输入框change
    onInputChange ({ detail }) {
      console.log('onInputChange')
      this.setData({
        inputValue: detail.value,
      })
      if (!detail.value) {
        this.handelAnimation(0)
        this.triggerEvent('addressInfo', {
          address: '',
          longitude: '',
          latitude: '',
        })
        setTimeout(() => {
          this.setData({
            nearbyAddressList: [],
          })
        }, 200)
      } else {
        this.getNearby(detail.value)
      }
    },
    // 输入框blur
    onInputBlur (e) {
      // return
      const onlyNamePickerList = this.data.nearbyAddressList
      // 判断用户输入是否是地址list中的一项
      let isIncludes = onlyNamePickerList.find(
        (item) => item.name === this.data.inputValue
      )
      if (!isIncludes) {
        this.data.addressItemInfo = {}
        this.triggerEvent('addressInfo', {
          address: '',
          longitude: '',
          latitude: '',
        })
        this.setData({
          inputValue: '',
        })
      }

      this.handelAnimation(0, false)
    },
    // 位置列表项单击
    onAddressTap (e) {
      const item = e.currentTarget.dataset.item
      //   this.setData({
      //     inputValue: item.name,
      //   })
      this.data.addressItemInfo.address = item.name
      this.data.addressItemInfo.longitude = item.location.split(',')[0]
      this.data.addressItemInfo.latitude = item.location.split(',')[1]
      this.triggerEvent('addressInfo', this.data.addressItemInfo)
    },
    getLocation () {
      return {
        address: this.data.inputValue, //地址
        longitude: this.data.longitude, //精度
        latitude: this.data.latitude, //纬度
      }
    },
    setRect () {
      let query = this.createSelectorQuery()
      query.select('.nearly-input').boundingClientRect((rect) => {
        if (!rect) {
          this.setRect()
        } else {
          this.setData({
            inputWidth: rect?.width,
          })
        }
      }).exec()
    },
    handelAnimation (height = 0, isShow = true) {
      setTimeout(() => {
        const animation = wx.createAnimation({
          duration: 300,
          timingFunction: 'ease-in-out',
        })
        animation.height(height).opacity(isShow ? 1 : 0).step()
        this.setData({
          animation: animation.export(),
        })
      })
    },
  },
})
