Component({
  lifetimes: {
    attached () {
      this.setRect()
    }
  },
  observers: {
    'pickerList': function (pickerList) {
      console.log('pickerList: ', pickerList);
      this.setData({
        isShowPicker: true
      })
    }
  },
  properties: {
    name: {
      type: String,
      value: ''
    },
    value: {
      type: String || Number,
      value: '' || 0
    },
    placeholder: {
      type: String,
      value: ''
    },
    label: {
      type: String,
      value: ''
    },
    type: {
      type: String,
      value: 'text'
    },
    pickerList: {
      type: Array,
      value: () => {
        return []
      }
    },
    isTextarea: {
      type: Boolean,
      value: false
    },
    showErrText: {
      type: Boolean,
      value: false
    },
    errText: {
      type: String,
      value: ''
    },
    isReadOnly: {
      type: Boolean,
      value: false
    }
  },
  data: {
    pickerVisible: false,
    isShowPicker: false
  },
  methods: {
    // 输入框内容更改时
    inputChange (e) {
      this.triggerEvent('inputChange', e.detail.value)
    },
    inputTap (e) {
      if (this.data.isShowPicker) {
        this.setData({
          pickerVisible: true
        })
      }
    },
    // 输入框获得焦点时
    inputFocus () {

    },
    // 输入框失去焦点
    inputBlur (e) {
      if (!this.data.isShowPicker) return

      // const onlyNamePickerList = this.data.pickerList.map(item => item.name)
      // const value = e.detail.value
      // console.log('value: ', value);
      // // 判断用户输入是否包括其中一项交通方式
      // let isIncludes = onlyNamePickerList.find(item => value.includes(item))
      // console.log('isIncludes: ', isIncludes);
      // if (isIncludes) {
      //   // 如果包括，则进一步判断用户输入内容是否符合onlyNamePickerList的字段
      //   let isFind = onlyNamePickerList.find(item => item === value)
      //   // 如果!isFind 说明输入和onlyNamePickerList的字段不一致,将仅包含的内容设置为输入内容
      //   if (!isFind) this.setData({ inputValue: isIncludes })
      // } else {
      //   this.setData({
      //     inputValue: ''
      //   })
      // }
      this.setData({
        pickerVisible: false
      })
    },
    // 输入框picker列表其中一项点击
    onPickerItemTap (e) {
      this.setData({
        inputValue: e.target.dataset.item.name,
        pickerVisible: false
      })
      this.triggerEvent('inputChange', e.target.dataset.item.name)
    },
    setRect () {
      let query = this.createSelectorQuery()
      query.select('.brutalist-input').boundingClientRect(rect => {
        if (!rect) {
          this.setRect()
        } else {
          this.setData({
            inputWidth: rect?.width,
          })
        }
      }).exec()
    },
  }
})