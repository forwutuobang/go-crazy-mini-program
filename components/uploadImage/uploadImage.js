import uploadImage from '@/utils/uploadImg';
Component({
  properties: {
    limitCount: { // 最大上传数量
      type: Number,
      value: 10
    },
    imageList: { //图片列表
      type: Array,
      value: () => {
        return []
      }
    },
    isReadOnly: {
      type: Boolean,
      value: false
    }
  },

  data: {
    apiImageUrlList: []
  },

  methods: {
    // 选择照片
    chooseImage () {
      wx.chooseMedia({
        count: this.properties.limitCount - this.data.imageList.length,
        mediaType: ['image'],
        sourceType: ['album', 'camera'],
        maxDuration: 30,
        camera: 'back',
        success: (ImageRes) => {
          wx.showLoading({
            title: '图片上传中...',
          })
          console.log('资源本地临时路径list', ImageRes);

          // 拿到资源本地临时路径list
          let tempFilePaths = ImageRes.tempFiles.map(item => item.tempFilePath)
          console.log('tempFilePaths: ', tempFilePaths);
          uploadImage(tempFilePaths).then(res => {
            if (res) {
              this.triggerEvent('change', [...res])
            }
          })
        }
      })
    },
    getImgList () {
      return this.data.imageList
    },
    // 删除图片
    deleteImage (e) {
      let value = e.currentTarget.dataset.item
      let imageList = this.data.imageList.filter(item => item !== value)
      this.setData({
        imageList
      })
      this.triggerEvent('change', imageList)
    },

    // 预览图片
    viewImage (e) {
      wx.previewMedia({
        sources: this.data.imageList.map(item => {
          return {
            url: item,
            type: 'image'
          }
        }),
        current: e.currentTarget.dataset.index,
        showmenu: true
      })
    }
  }
})

