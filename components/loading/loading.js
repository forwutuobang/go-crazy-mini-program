
Component({
  behaviors: [],
  properties: {
    visible: {
      type: Boolean,
      value: null
    }
  },
  data: {
    // visible: true,
    loadingText: "努力加载中..."
  },
  lifetimes: {
    created () {

    },
    attached () {

    },
    moved () {

    },
    detached () {

    },
  },
  methods: {
    handleOverlayClick (e) {
      // this.setData({
      //   visible: e.detail.visible,
      // });
      return
    },
    showLoading () {
      this.setData({
        visible: true
      })
    }
  },
});