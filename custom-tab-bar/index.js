Component({
  data: {
    selected: '/pages/index/index',
    list: [
      { value: '/pages/index/index', icon: 'map-location', },
      {
        value: '/pages/createTemp/createTemp',
        icon: {
          name: 'plus',
          color: '#fff'
        }
      },
      { value: '/pages/user/user', icon: 'user' },
    ],
  },
  methods: {
    onChange (e) {
      const url = e.detail.value
      if (this.data.selected === url) {
        let pages = getCurrentPages();
        if (pages[0].route === 'pages/index/index') {
          pages[0].selectComponent("#home-page").querySearch({ size: 10, current: 1 }, null, true, true)
        }
      }
      if (url === '/pages/createTemp/createTemp') {
        wx.navigateTo({
          url
        })
      } else {
        wx.switchTab({
          url
        })
      }

      // wx.setStorageSync('selected', e.detail.value)
    },
  },
});
